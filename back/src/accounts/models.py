from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
import datetime


class Rank(models.Model):
    number = models.IntegerField(unique=True)
    title = models.CharField(max_length=200)
    description = models.TextField()
    reward_point = models.IntegerField(default=0)

    def __str__(self):
        return "{}".format(self.title)


class User(AbstractUser):
    # bio
    bio = models.TextField(max_length=500, blank=True, null=True)
    user_avatar = models.ImageField(
        upload_to='profile_avatar/',
        default='profile_avatar/default.png',
        blank=True)
    birthday = models.DateField(blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    # security
    last_password_update = models.DateTimeField(auto_now_add=True)
    # mini game
    last_mini_game_date = models.DateTimeField(default=timezone.now() - datetime.timedelta(minutes=31))
    current_mini_game_round = models.PositiveIntegerField(default=1)
    mini_game_attempt_number = models.PositiveIntegerField(default=1)
    # online game
    online_game_total_games = models.IntegerField(default=0)
    online_game_count_of_wins = models.IntegerField(default=0)
    # statistics
    points = models.IntegerField(default=0)
    rank = models.ForeignKey(Rank, on_delete=models.CASCADE, null=True)
    rank_progress = models.IntegerField(default=100)

    @property
    def is_mini_game_available(self):
        if self.current_mini_game_round == 1 and timezone.now() - self.last_mini_game_date < datetime.timedelta(minutes=30):
            return False
        return True
