"""znatok URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from accounts import views as accounts_views
from exam import views as exam_views
from reviews import views as reviews_views
from game import views as game_views
from mini_game import views as mini_game_views

urlpatterns = [
    path('admin/', admin.site.urls),

    # user
    path('api/signup/', accounts_views.signup),
    path('api/login/', accounts_views.signin),
    path('api/logout/', accounts_views.signout),
    path('api/profile/', accounts_views.profile),
    path('api/init/', accounts_views.init),
    path('api/activate/<uidb64>/<token>/', accounts_views.activate, name='activate'),

    # tickets
    path('api/tickets/<category>/', exam_views.ticket_list),
    path('api/ticket/<pk>/', exam_views.ticket_detail),
    path('api/question/', exam_views.question_list),

    #game
    path('api/game/confirm/', game_views.confirm_game),
    path('api/game/create/', game_views.create_game),
    path('api/game/answer/', game_views.check_answers),

    # mini game
    path('api/mini_game/load/', mini_game_views.load),
    path('api/mini_game/check_answer/', mini_game_views.answer),

    # review
    path('api/review/', reviews_views.review_list),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
