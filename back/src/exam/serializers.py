from rest_framework import serializers
from exam.models import Theme, Ticket, Question, QuestionResult
import random
from django.db.models import F, Q, Count


class QuestionSerializer(serializers.ModelSerializer):
    answers = serializers.SerializerMethodField()
    result = serializers.SerializerMethodField()

    class Meta:
        model = Question
        fields = ('id', 'number', 'picture', 'question', 'answers', 'result', 'comment')

    @staticmethod
    def get_answers(obj):
        answers = [obj.answer1, obj.answer2]
        if obj.answer3:
            answers.append(obj.answer3)
        if obj.answer4:
            answers.append(obj.answer4)
        if obj.answer5:
            answers.append(obj.answer5)
        random.shuffle(answers)
        return answers

    def get_result(self, obj):
        request = self.context.get('request')
        user = request.user
        if user.is_authenticated:
            result = QuestionResult.objects.filter(question=obj, user=user).first()
            if result is None:
                return
            else:
                return {'is_true': result.is_true, 'user_answer': result.user_answer, 'true_answer': obj.answer1}

        else:
            ticket_id_str = str(obj.ticket_id)
            question_id_str = str(obj.pk)
            if request.session.get('results'):
                if ticket_id_str in request.session['results']:
                    if question_id_str in request.session['results'][ticket_id_str]:
                        return request.session['results'][ticket_id_str][question_id_str]
            return None


class TicketListSerializer(serializers.ModelSerializer):
    ticket_result = serializers.SerializerMethodField()

    class Meta:
        model = Ticket
        fields = ('id', 'number', 'ticket_result')

    def get_ticket_result(self, obj):
        request = self.context.get('request')
        user = request.user
        if user.is_authenticated:
            return QuestionResult.objects.filter(question__ticket=obj, user=user).aggregate(
                true=Count('pk', filter=Q(user_answer=F('question__answer1'))),
                false=Count('pk', filter=~Q(user_answer=F('question__answer1'))))
        else:
            ticket_id_str = str(obj.pk)
            if request.session.get('results'):
                if ticket_id_str in request.session['results']:
                    true = 0
                    false = 0
                    for v in request.session['results'][ticket_id_str].values():
                        if v['is_true']:
                            true += 1
                        else:
                            false += 1
                    return {'true': true, 'false': false}
        return {'true': 0, 'false': 0}


class TicketSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True)

    class Meta:
        model = Ticket
        fields = ('id', 'number', 'questions')


class ThemeSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True)

    class Meta:
        model = Theme
        fields = ('id', 'number', 'title', 'questions')
