from rest_framework.decorators import api_view
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework import status
from .models import Ticket, Question, Theme, QuestionResult
from .serializers import TicketSerializer, QuestionSerializer, ThemeSerializer, TicketListSerializer


@api_view(['GET'])
def ticket_detail(request, pk):
    if request.method == 'GET':
        ticket = get_object_or_404(Ticket, pk=pk)
        serializer = TicketSerializer(instance=ticket, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def ticket_list(request, category):
    if request.method == 'GET':
        tickets = Ticket.objects.filter(category=category)
        serializer = TicketListSerializer(tickets, context={'request': request}, many=True, allow_null=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST', 'GET'])
def question_list(request):
    if request.method == 'POST':
        user = request.user
        question_id = request.data.get('id')
        user_answer = request.data.get('answer')
        question_source = request.data.get('question_source')
        question = get_object_or_404(Question, pk=question_id)
        if user_answer is None or user_answer not in (
                question.answer1, question.answer2, question.answer3, question.answer4,
                question.answer5) or question_source not in (
                QuestionResult.TICKET_SOURCE, QuestionResult.THEME_SOURCE, QuestionResult.MARATHON_SOURCE):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        true_answer = question.answer1
        is_true = user_answer == true_answer
        if user.is_authenticated:
            question_result = user.questionresult_set.filter(question_source=question_source, question_id=question_id).first()
            if question_result and question_result.is_true:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                QuestionResult.objects.update_or_create(user=user, question=question, question_source=question_source, user_answer=user_answer)
            if is_true:
                user.points += 1
                user.save()
            points = user.points
        else:
            ticket_id_str = str(question.ticket_id)
            question_id_str = str(question_id)
            if not request.session.get('results'):
                request.session['results'] = {ticket_id_str: {
                    question_id_str: {'is_true': is_true, 'user_answer': user_answer, 'true_answer': true_answer}}}
            elif ticket_id_str not in request.session['results']:
                request.session['results'][ticket_id_str] = {
                    question_id_str: {'is_true': is_true, 'user_answer': user_answer, 'true_answer': true_answer}}
            elif question_id_str in request.session['results'][ticket_id_str] and \
                    request.session['results'][ticket_id_str][question_id_str]['user_answer'] == true_answer:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                request.session['results'][ticket_id_str].update(
                    {question_id_str: {'is_true': is_true, 'user_answer': user_answer, 'true_answer': true_answer}})
            if is_true:
                if request.session.get('points'):
                    request.session['points'] += 1
                else:
                    request.session['points'] = 1
            else:
                if not request.session.get('points'):
                    request.session['points'] = 0
            points = request.session['points']
            request.session.modified = True
        return Response({'is_true': is_true, 'true_answer': true_answer, 'user_answer': user_answer, 'points': points}, status=status.HTTP_200_OK)
