from django.db import models
from accounts.models import User


class Theme(models.Model):
    number = models.IntegerField()
    title = models.TextField(blank=True, null=True)


class Ticket(models.Model):
    number = models.IntegerField()
    category = models.IntegerField()


class Question(models.Model):
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE, related_name='questions')
    number = models.IntegerField()
    theme = models.ForeignKey(Theme, on_delete=models.CASCADE, related_name='questions')
    picture = models.ImageField(upload_to='images_for_quests/')
    question = models.TextField(max_length=2000)
    answer1 = models.CharField(max_length=500)
    answer2 = models.CharField(max_length=500)
    answer3 = models.CharField(max_length=500, blank=True, null=True)
    answer4 = models.CharField(max_length=500, blank=True, null=True)
    answer5 = models.CharField(max_length=500, blank=True, null=True)
    comment = models.TextField(max_length=4000)


class QuestionResult(models.Model):
    TICKET_SOURCE = 't'
    THEME_SOURCE = 'th'
    MARATHON_SOURCE = 'm'

    QUESTION_SOURCES = (('Билеты', TICKET_SOURCE),
                     ('По темам', THEME_SOURCE),
                     ('Марафон', MARATHON_SOURCE))

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    question_source = models.CharField(max_length=2, choices=QUESTION_SOURCES)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    user_answer = models.CharField(max_length=500, blank=True, null=True)

    @property
    def is_true(self):
        return self.user_answer == self.question.answer1
