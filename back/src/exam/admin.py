from django.contrib import admin
from .models import Question, Theme, Ticket, QuestionResult


admin.site.register(Question)
admin.site.register(Ticket)
admin.site.register(Theme)
admin.site.register(QuestionResult)

