from django.db import models


class Round(models.Model):
    number = models.PositiveIntegerField()
    title = models.TextField(blank=True, null=True)
    question = models.TextField()


class Sign(models.Model):
    round = models.ForeignKey(Round, on_delete=models.CASCADE)
    picture = models.ImageField(upload_to='mini_game_picture/', blank=True)
