from rest_framework import serializers
from .models import MiniGame


class MiniGameSerializer(serializers.ModelSerializer):
    class Meta:
        model = MiniGame
        fields = ()