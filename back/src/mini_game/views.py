import random

from django.shortcuts import get_object_or_404

from mini_game.models import Sign, Round
from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from mini_game.utils import is_anon_mini_game_available


def get_sign_for_round(round_number):
    current_round_sign_ids = Sign.objects.filter(round__number=round_number).all().values_list('id', flat=True)
    other_sign_ids = Sign.objects.exclude(round=round_number).all().values_list('id', flat=True)

    random.shuffle(current_round_sign_ids)
    random.shuffle(other_sign_ids)

    game_ids = current_round_sign_ids[:3] + other_sign_ids[:5]
    random.shuffle(game_ids)

    return Sign.objects.filter(pk__in=game_ids).all()


@api_view(['POST'])
def load(request):
    user = request.user

    if user.is_authenticated:
        if not user.is_mini_game_available:
            timer = user.last_mini_game_date + timezone.timedelta(minutes=30)

            return Response(data={'ready': False, 'timer': timer}, status=status.HTTP_200_OK)

        round_number = user.current_mini_game_round
        attempt_number = user.mini_game_attempt_number
    else:
        if not is_anon_mini_game_available(request):
            timer = timezone.datetime.fromtimestamp(request.session['last_mini_game']['last_date']) + timezone.timedelta(minutes=30)

            return Response(data={'ready': False, 'timer': timer}, status=status.HTTP_200_OK)
        round_number = request.session['last_mini_game']['current_round']
        attempt_number = request.session['last_mini_game']['attempt_number']

    game_round = get_object_or_404(Round, number=round_number)
    signs = list(get_sign_for_round(round_number).values('id', 'picture'))

    data = {'ready': True, 'attempt_number': attempt_number, 'signs': signs, 'question': game_round.question}

    return Response(data=data, status=status.HTTP_200_OK)


@api_view(['POST'])
def answer(request):
    user = request.user

    sign_ids = request.data.get('sign')

    is_true = None

    if user.is_authenticated:
        if not user.is_mini_game_available:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        round_number = user.current_mini_game_round
        is_true = Sign.objects.filter(pk__in=sign_ids, round__number=round_number).count() == 3

        if is_true:
            user.points += 10

            if user.last_mini_game_round == 5:
                user.last_mini_game_round = 1
            else:
                user.last_mini_game_round += 1

        else:
            if user.mini_game_attempt_number == 3:
                user.mini_game_attempt_number = 1

                if user.last_mini_game_round == 5:
                    user.last_mini_game_round = 1
                else:
                    user.last_mini_game_round += 1

        user.last_mini_game_date = timezone.now()
        user.save()

    else:
        if not is_anon_mini_game_available(request):
            return Response(status=status.HTTP_400_BAD_REQUEST)

        round_number = request.session['last_mini_game']['current_round']
        is_true = Sign.objects.filter(pk__in=sign_ids, round__number=round_number).count() == 3

        if is_true:
            request.session['points'] += 10

            if round_number == 5:
                request.session['last_mini_game']['current_round'] = 1
            else:
                request.session['last_mini_game']['current_round'] += 1

        else:
            if request.session['last_mini_game']['attempt_number'] == 3:
                request.session['last_mini_game']['attempt_number'] = 1

                if request.session['last_mini_game']['current_round'] == 5:
                    request.session['last_mini_game']['current_round'] = 1
                else:
                    request.session['last_mini_game']['current_round'] += 1

        request.session['last_mini_game']['last_game'] = timezone.now()
        request.session.modified = True

    return Response(data={'right_answer': is_true}, status=status.HTTP_200_OK)
