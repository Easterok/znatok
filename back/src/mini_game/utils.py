from django.utils import timezone


def is_anon_mini_game_available(request):
    if request.session.get('last_mini_game'):
        current_round = request.session['last_mini_game'].get('current_round')
        last_game_timestamp = request.session['last_mini_game'].get('last_date')
        last_game_date = timezone.make_aware(timezone.datetime.fromtimestamp(last_game_timestamp))

        return not (current_round == 1 and timezone.now() - last_game_date  < timezone.timedelta(minutes=30))
    else:

        request.session['last_mini_game'] = {'current_round': 1, 'attempt_number': 1,
                                             'last_date': (timezone.now() - timezone.timedelta(minutes=31)).timestamp()}
        return True
