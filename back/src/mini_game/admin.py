from django.contrib import admin
from .models import Sign, Round

admin.site.register(Round)
admin.site.register(Sign)

