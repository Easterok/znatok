from django.core.paginator import Paginator, EmptyPage
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework import status
from .models import Review
from .serializers import ReviewSerializer


@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticatedOrReadOnly, ))
def review_list(request):
    if request.method == 'GET':
        page = request.GET.get('page', 1)
        reviews = Review.objects.all()
        p = Paginator(reviews, 3)
        try:
            page = p.page(int(page))
        except (EmptyPage, ValueError):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        serializer = ReviewSerializer(page.object_list, many=True)
        return Response(data={'has_next_page': page.has_next(), 'data': serializer.data}, status=status.HTTP_200_OK)

    if request.method == 'POST':
        serializer = ReviewSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
