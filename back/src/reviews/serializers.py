from rest_framework import serializers
from .models import Review
from accounts.serializers import UserShortSerializer


class ReviewSerializer(serializers.ModelSerializer):
    published_at = serializers.CharField(read_only=True)
    user = UserShortSerializer(read_only=True)

    class Meta:
        model = Review
        fields = ('user', 'text', 'published_at', 'rating')
