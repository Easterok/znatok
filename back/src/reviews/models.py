from django.db import models
from accounts.models import User


class Review(models.Model):
    CHOICES = ((1, 'Плохо'),
               (2, 'Пойдет'),
               (3, 'Нормально'),
               (4, 'Хорошо'),
               (5, 'Отлично'))

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=200)
    published_at = models.DateTimeField(auto_now_add=True)
    rating = models.IntegerField(choices=CHOICES)

    def __str__(self):
        return "id: {}, text: {}".format(self.id, self.text[0:50])

    class Meta:
        ordering = ['-published_at', '-rating']
