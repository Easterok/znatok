from django.db import models
from accounts.models import User
from exam.models import Question


class Game(models.Model):
    ALLOWED_BETS = (0, 50, 100, 250, 500, 1000)

    STATUS_WAITING_APPROVE = 'w'
    STATUS_PLAYING = 'p'
    STATUS_FINISHED = 'f'

    GAME_STATUSES = (('Ожидание подтверждения', STATUS_WAITING_APPROVE),
                     ('Идет игра', STATUS_PLAYING),
                     ('Игра завершена', STATUS_FINISHED))

    status = models.CharField(max_length=1, choices=GAME_STATUSES, default=STATUS_WAITING_APPROVE)
    users = models.ManyToManyField(User, through='UserGame', related_name='games')
    questions = models.ManyToManyField(Question, related_name='games')
    bet = models.IntegerField(default=0)


class UserGame(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_games')
    game = models.ForeignKey(Game, on_delete=models.CASCADE, related_name='user_games')
    score = models.IntegerField(default=0)
    is_confirm = models.BooleanField(default=False)
    is_finished = models.BooleanField(default=False)


# class QuestionGameResult(models.Model):
#     user_game = models.ForeignKey(UserGame, on_delete=models.CASCADE)
#     question = models.ForeignKey(Question, on_delete=models.CASCADE)
#     user_answer = models.CharField(max_length=128)
