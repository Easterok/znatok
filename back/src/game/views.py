import random
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from exam.models import Question
from exam.serializers import QuestionSerializer
from game.matchmaker import Matchmaker
from game.models import Game
from django.conf import settings

from game.utils import delay

matchmaker = Matchmaker()


@api_view(['POST'])
def create_game(request):
    bet = request.data.get('bet')

    if bet in Game.ALLOWED_BETS:
        matchmaker.add_user(request.user, bet)

        return Response(status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def confirm_game(request):
    game_id = request.data.get('game_id')
    game = get_object_or_404(Game, pk=game_id, status=Game.STATUS_WAITING_APPROVE)
    user_game = get_object_or_404(game.user_games, user=request.user)
    user_game.is_confirm = True
    user_game.save()

    if not game.user_games.filter(is_confirm=False).exists():
        question_ids = list(Question.objects.all().values_list('id', flat=True))
        random.shuffle(question_ids)
        questions = Question.objects.filter(pk__in=question_ids[:10])
        game.questions.set(questions)
        game.status = game.STATUS_PLAYING
        game.save()

        data = QuestionSerializer(questions, context={'request': request}, many=True).data

        for user in game.users.all():
            settings.CENTRIFUGO_CLIENT.publish('#{}'.format(user.pk), {'status': 'playing', 'questions': data})

        @delay(60 * 5.5)
        def on_game_timeout():
            if game.user_games.filter(is_finished=False).exists():
                for user_game in game.user_games.all():
                    if user_game.is_finished:
                        game.status = Game.STATUS_FINISHED
                        game.save()

                        user_game.user.online_game_total_games += 1
                        user_game.user.online_game_count_of_wins += 1
                        user_game.user.points += game.bet
                        user_game.user.save()

                        settings.CENTRIFUGO_CLIENT.publish('#{}'.format(user_game.user.pk),
                                                           {'status': 'enemy answer timeout', 'score': user_game.score})
                    else:
                        user_game.user.points -= game.bet
                        user_game.user.save()

                        settings.CENTRIFUGO_CLIENT.publish('#{}'.format(user_game.user.pk),
                                                           {'status': 'self answer timeout'})
                print('Answer TimeOut')

        on_game_timeout()

    return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
def check_answers(request):
    game_id = request.data.get('game_id')
    answers = request.data.get('answers')

    game = get_object_or_404(Game, pk=game_id, status=Game.STATUS_PLAYING)
    questions = game.questions.all()
    user_game = game.user_games.filter(user=request.user)[0]

    for answer in answers:
        question_id = answer.get('question_id')
        user_answer = answer.get('answer')
        question = get_object_or_404(questions, pk=question_id)

        if user_answer == question.answer1:
            user_game.score += 1

    user_game.is_finished = True
    user_game.save()

    if not game.user_games.filter(is_finished=False).exists():
        game.status = Game.STATUS_FINISHED
        game.save()

        second_user_game = game.user_games.exclude(pk=user_game.pk).first()

        user_game.user.online_game_total_games += 1
        second_user_game.user.online_game_total_games += 1

        if user_game.score > second_user_game.score:
            user_game.user.online_game_count_of_wins += 1
            user_game.user.points += game.bet
            second_user_game.user.points -= game.bet

            settings.CENTRIFUGO_CLIENT.publish('#{}'.format(user_game.user.pk),
                                               {'status': 'won', 'score': user_game.score,
                                                'enemy_score': second_user_game.score})
            settings.CENTRIFUGO_CLIENT.publish('#{}'.format(second_user_game.user.pk),
                                               {'status': 'lost', 'score': second_user_game.score,
                                                'enemy_score': user_game.score})
        elif user_game.score < second_user_game.score:
            second_user_game.user.online_game_count_of_wins += 1
            user_game.user.points -= game.bet
            second_user_game.user.points += game.bet

            settings.CENTRIFUGO_CLIENT.publish('#{}'.format(user_game.user.pk),
                                               {'status': 'lost', 'score': user_game.score,
                                                'enemy_score': second_user_game.score})
            settings.CENTRIFUGO_CLIENT.publish('#{}'.format(second_user_game.user.pk),
                                               {'status': 'won', 'score': second_user_game.score,
                                                'enemy_score': user_game.score})
        else:
            settings.CENTRIFUGO_CLIENT.publish('#{}'.format(user_game.user.pk),
                                               {'status': 'drawn', 'score': user_game.score})
            settings.CENTRIFUGO_CLIENT.publish('#{}'.format(second_user_game.user.pk),
                                               {'status': 'drawn', 'score': second_user_game.score})

        user_game.user.save()
        second_user_game.user.save()
    else:
        settings.CENTRIFUGO_CLIENT.publish('#{}'.format(user_game.user.pk),
                                           {'status': 'waiting enemy', 'score': user_game.score})

    return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
def disconnect(request):
    game_id = request.data.get('game_id')

    return Response(status=status.HTTP_200_OK)
