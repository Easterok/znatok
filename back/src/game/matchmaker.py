from game.models import Game
from game.utils import delay
from django.conf import settings


class Matchmaker:
    user_queue = {}

    @staticmethod
    def _start_confirm_check(users, bet):
        game = Game.objects.create(bet=bet)

        for user in users:
            game.user_games.create(user=user)
            print(user)
            settings.CENTRIFUGO_CLIENT.publish('#{}'.format(user.pk), {'status': 'confirm', 'game_id': game.pk})
        
        @delay(10.)
        def check_confirm():
            if game.user_games.filter(is_confirm=False).exists():
                for user_game in game.user_games.all():
                    if user_game.is_confirm:
                        settings.CENTRIFUGO_CLIENT.publish('#{}'.format(user_game.user.pk), {'status': 'enemy confirm timeout'})
                    else:
                        settings.CENTRIFUGO_CLIENT.publish('#{}'.format(user_game.user.pk), {'status': 'self confirm timeout'})
                print('Confirm TimeOut')
                game.delete()

        check_confirm()

    def add_user(self, first_user, bet):
        if bet in self.user_queue:
            second_user = self.user_queue.pop(bet)
            self._start_confirm_check([first_user, second_user], bet)
        else:
            self.user_queue[bet] = first_user
