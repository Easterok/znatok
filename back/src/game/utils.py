import threading
from functools import wraps


def delay(delay_time=0.):
    def wrap(f):
        @wraps(f)
        def delayed(*args, **kwargs):
            timer = threading.Timer(delay_time, f, args, kwargs)
            timer.start()
        return delayed
    return wrap
