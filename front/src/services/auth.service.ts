import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {tap} from 'rxjs/operators';
import {INewUser, IUser} from '../models/new-user.model';
import {UserStateService} from './user-state.service';
import {LeadersService} from '../modules/app/modules/main-block/modules/leaders/leaders.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(
        private readonly http: HttpClient,
        private readonly userStateService: UserStateService,
        private readonly leadersService: LeadersService
    ) {}

    registration$(user: INewUser): Observable<any> {
        return this.http.post<IUser>(`/api/signup/`, user, {withCredentials: true});
    }

    login$(user: any): Observable<IUser> {
        return this.http.post<IUser>('/api/login/', user, {withCredentials: true}).pipe(
            tap(user => (this.userStateService.user = user)),
            tap(({points}) => (this.userStateService.points = points))
        );
    }

    logout$(): Observable<any> {
        return this.http.post<any>('/api/logout/', {}, {withCredentials: true}).pipe(
            tap(() => (this.userStateService.user = null)),
            tap(() => (this.userStateService.points = 0))
        );
    }

    init$(): Observable<any> {
        return this.http.get<any>('/api/init/', {withCredentials: true}).pipe(
            tap(({user_data: user}) => (this.userStateService.user = user)),
            tap(({points}) => (this.userStateService.points = points)),
            tap(({leaders}) => (this.leadersService.leaders = leaders))
        );
    }
}
