import {Injectable} from '@angular/core';
import {fromEvent} from 'rxjs';
import {distinctUntilChanged, filter, tap} from 'rxjs/operators';
import {KEY_CODES} from '../shared/consts/consts';
import {some} from 'lodash';
import {Observable} from 'rxjs/Rx';

@Injectable({
    providedIn: 'root'
})
export class KeyBoardEventsService {
    private previousCode: string | null;

    constructor() {
        this.subscribeOnKeyUp();
    }

    subscribeOnKeyDown$(): Observable<KeyboardEvent> {
        return fromEvent(document, 'keydown').pipe(
            filter(({target}) => target === document.body),
            filter(({code}: KeyboardEvent) => {
                return Object.keys(KEY_CODES).some(key => KEY_CODES[key] === code);
            }),
            distinctUntilChanged(({code}: KeyboardEvent) => this.previousCode === code),
            tap(({code}: KeyboardEvent) => (this.previousCode = code)),
            tap((event: KeyboardEvent) => event.preventDefault())
        );
    }

    private subscribeOnKeyUp() {
        fromEvent(document, 'keyup').subscribe((event: KeyboardEvent) => {
            event.preventDefault();

            this.previousCode = null;
        });
    }
}
