import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs/Rx';
import {LocalStorageService} from './local-storage.service';
import {IUser} from '../models/new-user.model';

const LOCAL_STORAGE_KEY = 'doubleClickOnAnswer';

@Injectable({
    providedIn: 'root'
})
export class UserStateService {
    private user$ = new BehaviorSubject<IUser>(null);
    private points$ = new BehaviorSubject<number>(0);
    private teamPoints$ = new BehaviorSubject<number>(0);
    private beepcoin$ = new BehaviorSubject<number>(0);
    private doubleClickOnAnswer: boolean;

    constructor(private localStorageService: LocalStorageService) {
        try {
            this.doubleClickOnAnswer = this.localStorageService.get(LOCAL_STORAGE_KEY) === 'true';
        } catch (error) {
            this.doubleClickOnAnswer = false;
            this.localStorageService.set(LOCAL_STORAGE_KEY, this.doubleClickOnAnswer);
        }
    }

    get _user$(): Observable<IUser | null> {
        return this.user$.asObservable();
    }

    set user(user: IUser | null) {
        user ? this.user$.next(user) : this.user$.next(null);
    }

    set points(points: number) {
        this.points$.next(points);
    }

    getPoints$(): Observable<number> {
        return this.points$.asObservable();
    }

    incPoints() {
        this.points = this.points$.getValue() + 1;
    }

    getTeamPoints$(): Observable<number> {
        return this.teamPoints$.asObservable();
    }

    getBeepCoin$(): Observable<number> {
        return this.beepcoin$.asObservable();
    }

    getDoubleClickOnAnswer(): boolean {
        return this.doubleClickOnAnswer;
    }

    setDoubleClickOnAnswer(isDouble: boolean) {
        this.doubleClickOnAnswer = isDouble;
        this.localStorageService.set(LOCAL_STORAGE_KEY, this.doubleClickOnAnswer);
    }
}
