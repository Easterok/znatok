import {Injectable} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class RouterService {
    constructor(private readonly router: Router, private readonly activatedRoute: ActivatedRoute) {}

    setUrl(commands: any[], queryParams?: Params) {
        this.router.navigate(commands, {queryParams, relativeTo: this.activatedRoute, queryParamsHandling: 'merge'});
    }
}
