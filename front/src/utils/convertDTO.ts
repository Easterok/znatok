const socketInfo = {
    gameId: 'game_id',
    questionId: 'question_id',
    attemptNumber: 'attempt_number',
    rightAnswer: 'right_answer',
    url: 'picture'
};

const convertObj = {...socketInfo};

export function convertToDTO(obj: any) {
    const isArray = Array.isArray(obj);
    const newObj = isArray ? [] : {};

    Object.keys(obj).forEach(key => {
        const dtoKey = convertObj[key] ? convertObj[key] : key;
        const value = typeof obj[key] === 'object' && obj[key] !== null ? convertToDTO(obj[key]) : obj[key];

        if (isArray) {
            (newObj as Array<any>).push(value);
        } else {
            newObj[dtoKey] = value;
        }
    });

    return newObj;
}

export function convertFromDTO<T>(obj: T): T {
    const isArray = Array.isArray(obj);
    const newObj = isArray ? [] : {};

    Object.keys(obj).forEach(key => {
        const frontKey = Object.keys(convertObj).find(convertKey => convertObj[convertKey] === key) || key;
        const value = obj[key];
        const frontValue = typeof value === 'object' && value !== null ? convertFromDTO(value) : value;

        if (isArray) {
            (newObj as Array<any>).push(frontValue);
        } else {
            newObj[frontKey] = frontValue;
        }
    });

    return newObj as T;
}
