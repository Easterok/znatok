import {IQuestion} from '@models/ticket.model';
import {OnlineGameActionsUnion, OnlineGameType} from '@store/actions/online-game.actions';

export enum OnlineGameEnum {
    DEFAULT = 'default',
    WAITING = 'waiting',
    CONFIRM = 'confirm',
    OK = 'ok',
    CANCEL = 'cancel',
    TIMEOUT = 'timeout',
    PLAYING = 'playing',
    ENEMY_ANSWER_TIMEOUT = 'enemy answer timeout',
    SELF_ANSWER_TIMEOUT = 'self answer timeout',
    WAITING_ENEMY = 'waiting enemy',
    WON = 'won',
    LOST = 'lost',
    DRAWN = 'drawn'
}

export const resultStatusesTypes = [OnlineGameEnum.WON, OnlineGameEnum.LOST, OnlineGameEnum.DRAWN];

export interface IOnlineGameState {
    process: OnlineGameEnum;
    gameId: number | null;
    questions: IQuestion[] | null;
}

const initialOnlineGameState: IOnlineGameState = {
    process: OnlineGameEnum.DEFAULT,
    gameId: null,
    questions: null
};

export function onlineGameReducer(state = initialOnlineGameState, action: OnlineGameActionsUnion): IOnlineGameState {
    switch (action.type) {
        case OnlineGameType.SET_PROCESS:
            return {...state, process: action.payload};
        case OnlineGameType.SET_GAME_ID:
            return {...state, gameId: action.payload};
        case OnlineGameType.SET_QUESTIONS:
            return {...state, questions: action.payload};
        case OnlineGameType.RESET:
            return initialOnlineGameState;
        default:
            return state;
    }
}
