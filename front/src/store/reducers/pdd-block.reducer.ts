import {Category, IPddBlocksState, PddBlocks} from '@store/models/state';
import {PddBlocksActionsType, PddBlocksActionsUnion} from '@store/actions/pdd-blocks.actions';

const initialBlocksState: IPddBlocksState = {
    activeBlock: PddBlocks.TICKETS,
    ticket: null,
    question: null,
    category: Category.ABM
};

export function pddBlocksReducer(state = initialBlocksState, action: PddBlocksActionsUnion): IPddBlocksState {
    switch (action.type) {
        case PddBlocksActionsType.LOAD:
            return action.payload;
        case PddBlocksActionsType.CHANGE_ACTIVE_BLOCK:
            return {...state, activeBlock: action.payload.type, question: null, ticket: null};
        case PddBlocksActionsType.SET_CATEGORY:
            return {...state, category: action.payload, question: null, ticket: null};
        case PddBlocksActionsType.CHOICE_TICKET:
            return {...state, ticket: action.payload, question: 1};
        case PddBlocksActionsType.CHOICE_QUESTION:
            return {...state, question: action.payload};
        default:
            return state;
    }
}
