import {ActionReducerMap, MetaReducer} from '@ngrx/store';
import {storeFreeze} from 'ngrx-store-freeze';
import {IState} from '@store/models/state';
import {pddBlocksReducer} from '@store/reducers/pdd-block.reducer';
import {environment} from '../../environments/environment';
import {onlineGameReducer} from '@store/reducers/online-game.reducer';

export const reducers: ActionReducerMap<IState> = {
    blocksState: pddBlocksReducer,
    onlineGame: onlineGameReducer
};

export const metaReducers: MetaReducer<any>[] = !environment.production ? [storeFreeze] : [];
