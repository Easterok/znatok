import {Category} from '@store/models/state';
import {Action} from '@ngrx/store';
import {ILink} from '@modules/app/modules/main-block/components/main-block/main-block.component';

export enum PddBlocksActionsType {
    LOAD = '[Pdd] Load',
    CHANGE_ACTIVE_BLOCK = '[Pdd] Change Active Block',
    SET_CATEGORY = '[Pdd] Set Category',
    CHOICE_TICKET = '[Pdd] Choice Ticket',
    CHOICE_QUESTION = '[Pdd] Choice Question'
}

export class LoadState implements Action {
    readonly type = PddBlocksActionsType.LOAD;

    constructor(public readonly payload: any) {}
}

export class ChangeActiveBlock implements Action {
    readonly type = PddBlocksActionsType.CHANGE_ACTIVE_BLOCK;

    constructor(public readonly payload: ILink) {}
}

export class SetCategory implements Action {
    readonly type = PddBlocksActionsType.SET_CATEGORY;

    constructor(public readonly payload: Category) {}
}

export class ChoiceTicket implements Action {
    readonly type = PddBlocksActionsType.CHOICE_TICKET;

    constructor(public readonly payload: number | null) {}
}

export class ChoiceQuestion implements Action {
    readonly type = PddBlocksActionsType.CHOICE_QUESTION;

    constructor(public readonly payload: number | null) {}
}

export type PddBlocksActionsUnion = LoadState | ChangeActiveBlock | SetCategory | ChoiceTicket | ChoiceQuestion;
