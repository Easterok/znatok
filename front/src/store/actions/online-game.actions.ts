import {Action} from '@ngrx/store';
import {OnlineGameEnum} from '@store/reducers/online-game.reducer';
import {IQuestion} from '@models/ticket.model';

export enum OnlineGameType {
    SET_PROCESS = '[Online game] Set process',
    SET_GAME_ID = '[Online game] Set game id',
    SET_QUESTIONS = '[Online game] Set questions',
    RESET = '[Online game] Reset'
}

export class SetProcess implements Action {
    readonly type = OnlineGameType.SET_PROCESS;

    constructor(public readonly payload: OnlineGameEnum) {}
}

export class SetGameId implements Action {
    readonly type = OnlineGameType.SET_GAME_ID;

    constructor(public readonly payload: number) {}
}

export class SetQuestions implements Action {
    readonly type = OnlineGameType.SET_QUESTIONS;

    constructor(public readonly payload: IQuestion[]) {}
}

export class Reset implements Action {
    readonly type = OnlineGameType.RESET;
}

export type OnlineGameActionsUnion = SetProcess | SetGameId | SetQuestions | Reset;
