import {IOnlineGameState} from '@store/reducers/online-game.reducer';

export interface IState {
    blocksState: IPddBlocksState;
    onlineGame: IOnlineGameState;
}

export interface IPddBlocksState {
    activeBlock: PddBlocks;
    ticket: number | null;
    question: number | null;
    category: Category;
}

export enum PddBlocks {
    THEORY = 'theory',
    TICKETS = 'tickets',
    EXAM = 'exam',
    THEMES = 'theme',
    MARATHON = 'marathon',
    MISTAKES = 'mistakes'
}

export enum Category {
    ABM,
    CD
}
