import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {OnlineGameType, Reset, SetProcess} from '@store/actions/online-game.actions';
import {delay, filter, map, tap, withLatestFrom} from 'rxjs/operators';
import {OnlineGameEnum, resultStatusesTypes} from '@store/reducers/online-game.reducer';
import {onlineGameSelectors} from '@store/selectors/online-game.selectors';
import {select, Store} from '@ngrx/store';
import {IState} from '@store/models/state';
import {OnlineGameService} from '@modules/app/modules/online-game/servises/online-game.service';
import {Observable} from 'rxjs';

@Injectable()
export class OnlineGameEffects {
    @Effect({dispatch: false})
    changeProcess$ = this.getProcess(OnlineGameEnum.OK).pipe(
        withLatestFrom(this.store.pipe(select(onlineGameSelectors.gameId))),
        tap(([_, gameId]) => this.onlineGameService.confirmGame(gameId).subscribe())
    );

    @Effect()
    changeProcess$_ = this.actions$.pipe(
        ofType(
            OnlineGameEnum.ENEMY_ANSWER_TIMEOUT,
            OnlineGameEnum.SELF_ANSWER_TIMEOUT,
            OnlineGameEnum.TIMEOUT,
            OnlineGameEnum.CANCEL
        ),
        map(() => new Reset())
    );

    @Effect()
    reset$ = this.actions$.pipe(
        ofType(...resultStatusesTypes),
        delay(7000),
        map(() => new Reset())
    );

    constructor(
        private readonly actions$: Actions,
        private readonly store: Store<IState>,
        private readonly onlineGameService: OnlineGameService
    ) {}

    private getProcess(processType: OnlineGameEnum): Observable<SetProcess> {
        return this.actions$.pipe(
            ofType<SetProcess>(OnlineGameType.SET_PROCESS),
            filter(({payload: process}) => process === processType)
        );
    }
}
