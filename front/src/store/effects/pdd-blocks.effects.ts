import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {tap, withLatestFrom} from 'rxjs/internal/operators';
import {
    ChangeActiveBlock,
    ChoiceQuestion,
    ChoiceTicket,
    PddBlocksActionsType,
    SetCategory
} from '@store/actions/pdd-blocks.actions';
import {RouterService} from '@services/router.service';
import {select, Store} from '@ngrx/store';
import {IPddBlocksState, IState, PddBlocks} from '@store/models/state';
import {blocksSelectors} from '@store/selectors/pdd-blocks.selectors';

@Injectable()
export class PddBlocksEffects {
    @Effect({dispatch: false})
    changeCategory$ = this.actions$.pipe(
        ofType<SetCategory>(PddBlocksActionsType.SET_CATEGORY),
        withLatestFrom(this.store.pipe(select(blocksSelectors.activeBlock))),
        tap(([{payload}, activeBlock]: [SetCategory, PddBlocks]) =>
            this.routerService.setUrl([activeBlock], {category: payload || null, question: null, ticket: null})
        )
    );

    @Effect({dispatch: false})
    changeActiveBlock$ = this.actions$.pipe(
        ofType<ChangeActiveBlock>(PddBlocksActionsType.CHANGE_ACTIVE_BLOCK),
        tap(({payload}) => {
            this.routerService.setUrl([payload.url], {question: null, ticket: null});
        })
    );

    @Effect({dispatch: false})
    choiceTicket$ = this.actions$.pipe(
        ofType(PddBlocksActionsType.CHOICE_TICKET),
        withLatestFrom(this.store.pipe(select(blocksSelectors.fullState))),
        tap(([_, {activeBlock, question, ticket}]: [ChoiceTicket, IPddBlocksState]) => {
            this.routerService.setUrl([`${activeBlock}/${ticket}`], {question});
        })
    );

    @Effect({dispatch: false})
    choiceQuestion$ = this.actions$.pipe(
        ofType<ChoiceQuestion>(PddBlocksActionsType.CHOICE_QUESTION),
        tap(({payload: question}) => this.routerService.setUrl([], {question}))
    );

    constructor(
        private readonly actions$: Actions,
        private readonly routerService: RouterService,
        private readonly store: Store<IState>
    ) {}
}
