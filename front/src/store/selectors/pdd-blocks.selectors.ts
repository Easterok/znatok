import {createFeatureSelector, createSelector} from '@ngrx/store';
import {IPddBlocksState} from '@store/models/state';

const selectFullState = createFeatureSelector<IPddBlocksState>('blocksState');

const selectCategory = createSelector(
    selectFullState,
    (state: IPddBlocksState) => state.category
);

const selectActiveBlock = createSelector(
    selectFullState,
    (state: IPddBlocksState) => state.activeBlock
);

const selectTicket = createSelector(
    selectFullState,
    (state: IPddBlocksState) => state.ticket
);

const selectQuestion = createSelector(
    selectFullState,
    (state: IPddBlocksState) => state.question
);

export const blocksSelectors = {
    fullState: selectFullState,
    category: selectCategory,
    activeBlock: selectActiveBlock,
    ticket: selectTicket,
    question: selectQuestion
};
