import {createFeatureSelector, createSelector} from '@ngrx/store';
import {IOnlineGameState} from '@store/reducers/online-game.reducer';

const ONLINE_GAME_NAME = 'onlineGame';

const selectFullState = createFeatureSelector<IOnlineGameState>(ONLINE_GAME_NAME);

const selectGameId = createSelector(
    selectFullState,
    ({gameId}) => gameId
);

const selectQuestions = createSelector(
    selectFullState,
    ({questions}) => questions
);

const selectProcess = createSelector(
    selectFullState,
    ({process}) => process
);

export const onlineGameSelectors = {
    fullState: selectFullState,
    gameId: selectGameId,
    questions: selectQuestions,
    process: selectProcess
};
