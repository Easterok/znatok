export enum KEY_CODES {
    SPACE = 'Space',
    ARROW_RIGHT = 'ArrowRight',
    ARROW_LEFT = 'ArrowLeft',
    // ARROW_UP = 'ArrowUp',
    // ARROW_DOWN = 'ArrowDown',
    DIGIT_1 = 'Digit1',
    DIGIT_2 = 'Digit2',
    DIGIT_3 = 'Digit3',
    DIGIT_4 = 'Digit4',
    DIGIT_5 = 'Digit5'
}
