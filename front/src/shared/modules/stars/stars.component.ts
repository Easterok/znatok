import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    forwardRef,
    HostBinding,
    HostListener,
    Input
} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {debounce} from 'lodash';

@Component({
    selector: 'pdd-stars',
    templateUrl: './stars.component.html',
    styleUrls: ['./stars.component.less'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => StarsComponent),
            multi: true
        }
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StarsComponent implements ControlValueAccessor {
    @Input()
    @HostBinding('class._disabled')
    disabled = true;
    stars: boolean[] = Array(5).fill(false);
    onChange = (rating: number) => {};
    onTouched = () => {};
    private rating = 5;

    constructor(private cd: ChangeDetectorRef) {}

    get value(): number {
        return this.stars.reduce((total, sparred) => {
            return total + (sparred ? 1 : 0);
        }, 0);
    }

    @HostListener('mouseleave')
    onMouseLeave() {
        debounce(() => this.setRating(this.rating), 60)();
    }

    hoverOnStar(rating: number) {
        debounce(() => this.setRating(rating), 50)();
    }

    clickOnStar(rating: number) {
        this.rating = rating;
        this.setRating(rating);
    }

    setRating(rating: number) {
        if (this.disabled) {
            return;
        }

        this.writeValue(rating);
    }

    writeValue(rating: number): void {
        this.stars = this.stars.map((_, i) => rating > i);
        this.onChange(this.value);
        this.cd.markForCheck();
    }

    registerOnChange(fn: (rating: number) => void): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }
}
