import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    Output,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import {KeyBoardEventsService} from '../../../services/key-board-events.service';
import {takeUntil, throttleTime} from 'rxjs/operators';
import {Subject} from 'rxjs/Rx';
import {KEY_CODES} from '../../consts/consts';
import {UserStateService} from '../../../services/user-state.service';
import {IQuestion} from '../../../models/ticket.model';

export type ChangeQuestionType = 'previous' | 'next' | 'reload' | number;

export interface IAnswer {
    id: number;
    answer: string;
}

@Component({
    selector: 'pdd-question-preview',
    templateUrl: './question-preview.component.html',
    styleUrls: ['./question-preview.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuestionPreviewComponent implements OnChanges, OnDestroy {
    @ViewChild('focus') focusElement: ElementRef;

    @Input() reload = false;

    @Input() question: IQuestion;

    @Input() hideHelp = false;

    @Input() rightAnswer: string;

    @Input() hideNext = false;

    @Input() setFocus = true;

    @Output() answer = new EventEmitter<IAnswer>();

    @Output() changeQuestion = new EventEmitter<ChangeQuestionType>();

    openText = false;

    buttonSelect: number;

    rightAnswerNumber: number;

    private destroy$ = new Subject<void>();

    private doubleClickCounter = 0;

    constructor(
        private keyBoardEventsService: KeyBoardEventsService,
        private cd: ChangeDetectorRef,
        private userStateService: UserStateService
    ) {
        this.keyBoardEventsService
            .subscribeOnKeyDown$()
            .pipe(
                throttleTime(300),
                takeUntil(this.destroy$)
            )
            .subscribe(this.makeAnswerByKeyCode.bind(this));
    }

    ngOnChanges() {
        if (!this.focusElement) {
            return;
        }

        if (this.question && this.question.result) {
            this.cd.markForCheck();

            return;
        }

        this.rightAnswer = null;
        this.rightAnswerNumber = null;
        this.buttonSelect = null;
        this.openText = false;
        this.cd.markForCheck();

        if (!this.setFocus) {
            return;
        }

        this.focusElement.nativeElement.scrollIntoView({block: 'center'});
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    toggleHelpText() {
        this.openText = !this.openText;
    }

    onClickChangeQuestion(type: ChangeQuestionType) {
        this.changeQuestion.emit(type);
    }

    emitAnswer(answerNumber: number) {
        if (this.rightAnswer) {
            return;
        }

        if (this.buttonSelect !== answerNumber) {
            this.doubleClickCounter = 0;
        }

        this.buttonSelect = answerNumber;
        this.cd.markForCheck();

        if (this.userStateService.getDoubleClickOnAnswer() && this.doubleClickCounter === 0) {
            this.doubleClickCounter += 1;

            return;
        }

        this.doubleClickCounter = 0;

        const answer = <IAnswer>{
            id: this.question.id,
            answer: this.question.answers[this.buttonSelect]
        };

        this.answer.emit(answer);
    }

    private makeAnswerByKeyCode({code}: KeyboardEvent) {
        if (code === KEY_CODES.SPACE || code === KEY_CODES.ARROW_RIGHT) {
            this.changeQuestion.emit('next');

            return;
        }

        if (code === KEY_CODES.ARROW_LEFT) {
            this.changeQuestion.emit('previous');

            return;
        }

        if (!(this.question && !this.question.result)) {
            return;
        }

        this.emitAnswer(<any>code.match(/\d/g)[0] - 1);
    }
}
