import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {QuestionPreviewComponent} from './question-preview.component';

@NgModule({
    imports: [CommonModule],
    declarations: [QuestionPreviewComponent],
    exports: [QuestionPreviewComponent]
})
export class QuestionPreviewModule {}
