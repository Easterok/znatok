import {ChangeDetectionStrategy, Component, HostBinding, Input} from '@angular/core';

type sizeType = 's' | 'm' | 'l';

type modeType = 'default' | 'success' | 'error' | 'primary' | 'inherit';

@Component({
    selector: 'pdd-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent {
    @Input()
    @HostBinding('attr._mode')
    mode: modeType = 'default';

    @Input()
    @HostBinding('attr._size')
    size: sizeType = 'm';

    @Input()
    @HostBinding('class._disabled')
    disabled = false;

    @Input()
    type = 'button';

    @Input()
    focusable = true;

    get computedFocusable(): boolean {
        return this.disabled ? false : this.focusable;
    }
}
