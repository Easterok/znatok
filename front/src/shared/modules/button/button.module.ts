import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonComponent} from './button.component';
import {FocusedModule} from '@shared/directives/focused/focused.module';

@NgModule({
    declarations: [ButtonComponent],
    imports: [CommonModule, FocusedModule],
    exports: [ButtonComponent]
})
export class ButtonModule {}
