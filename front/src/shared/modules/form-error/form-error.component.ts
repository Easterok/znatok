import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges} from '@angular/core';
import {AbstractControl} from '@angular/forms';

const DEFAULT_MESSAGE = 'Поле заполнено неправильно';

@Component({
    selector: 'pdd-form-error',
    templateUrl: './form-error.component.html',
    styleUrls: ['./form-error.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormErrorComponent implements OnChanges {
    @Input() control: AbstractControl;
    message = DEFAULT_MESSAGE;

    constructor(private cd: ChangeDetectorRef) {}

    ngOnChanges() {
        this.control.valueChanges.subscribe(() => this.cd.markForCheck());
    }

    get errors(): string {
        if (this.control.untouched || this.control.pristine) {
            return;
        }

        const key = Object.keys(this.control.errors)[0];

        return this.control.errors[key];
    }
}
