import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
    selector: 'pdd-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationComponent {
    @Input() activeIndex = 0;
    @Input() max = 10;

    private length = this.max > 10 ? 10 : this.max;
    private start = 0;
    private active = 0;

    get pagination(): Array<any> {
        return Array(this.length - this.start);
    }

    get hasPrevious(): boolean {
        return this.start > 0;
    }

    get hasNext(): boolean {
        return this.length < this.max;
    }

    get startIndex(): number {
        return this.start;
    }

    get activeItem(): number {
        return this.active;
    }

    onClick(index: string | number) {
        if (index === 'next') {
            this.start = this.length;
            this.length = this.length + 10 > this.max ? this.max : this.length + 10;
            this.active = this.start;

            return;
        }

        if (index === 'previous') {
            this.start = this.start - 10 > 0 ? this.start - 10 : 0;
            this.length = this.start + 10;
            this.active = this.length - 1;

            return;
        }

        this.active = index as number;
    }
}
