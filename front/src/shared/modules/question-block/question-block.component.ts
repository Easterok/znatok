import {ChangeDetectionStrategy, Component, ElementRef, HostBinding, HostListener, Input} from '@angular/core';

@Component({
    selector: 'pdd-question-block',
    templateUrl: './question-block.component.html',
    styleUrls: ['./question-block.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuestionBlockComponent {
    @Input() information: string;

    @Input()
    @HostBinding('class._bordered')
    bordered = true;

    open = false;

    constructor(private elementRef: ElementRef) {}

    @HostListener('document:click', ['$event'])
    onClick(event: MouseEvent) {
        if (!this.open) {
            return;
        }

        const nativeElement = this.elementRef.nativeElement;
        const content = nativeElement.querySelector('.information-block_fully');
        const button = nativeElement.querySelector('.information-block');
        const inside = button.contains(event.target) || content.contains(event.target);

        if (inside) {
            return;
        }

        this.open = false;
    }

    toggleOpen() {
        this.open = !this.open;
    }
}
