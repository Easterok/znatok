import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {QuestionBlockComponent} from './question-block.component';

@NgModule({
    imports: [CommonModule],
    declarations: [QuestionBlockComponent],
    exports: [QuestionBlockComponent]
})
export class QuestionBlockModule {}
