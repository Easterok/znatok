import {ChangeDetectionStrategy, Component, HostBinding, Input} from '@angular/core';

type size = 's' | 'm' | 'xl';

@Component({
    selector: 'pdd-progress-bar',
    templateUrl: './progress-bar.component.html',
    styleUrls: ['./progress-bar.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgressBarComponent {
    @Input() done: number;

    @Input() error: number;

    @Input() max = 100;

    @Input()
    @HostBinding('attr._size')
    size: size = 'm';

    @Input()
    @HostBinding('class._grid')
    showGrid = false;

    get donePercent(): string {
        return `${((this.done > this.max ? this.max : this.done) / this.max) * 100}%`;
    }

    get errorPercent(): string {
        if (this.error > this.max) {
            return '100%';
        }

        return `${((this.done + this.error) / this.max) * 100}%`;
    }
}
