import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonListComponent} from './button-list.component';

@NgModule({
    imports: [CommonModule],
    declarations: [ButtonListComponent],
    exports: [ButtonListComponent]
})
export class ButtonListModule {}
