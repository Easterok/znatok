import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {IQuestion} from '../../../models/ticket.model';

@Component({
    selector: 'pdd-button-list',
    templateUrl: './button-list.component.html',
    styleUrls: ['./button-list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonListComponent {
    @Input() start = 1;
    @Input() max: number;
    @Input() questions: IQuestion[];
    @Input() current = 1;

    @Output() buttonNumber = new EventEmitter<number>();

    private maximum = this.start + 20 - 1;

    onClickButton(index: number | string) {
        if (index === 'next') {
            this.start = this.maximum + 1;
            this.maximum = this.start + 20 - 1;
            this.current = this.start;
            this.buttonNumber.emit(this.current);

            return;
        }

        if (index === 'previous') {
            this.start = this.start - 20 > 0 ? this.start - 20 : 1;
            this.maximum = this.start + 20 - 1;
            this.current = this.maximum;
            this.buttonNumber.emit(this.current);

            return;
        }

        this.current = index as number;
        this.buttonNumber.emit(this.current);
    }
}
