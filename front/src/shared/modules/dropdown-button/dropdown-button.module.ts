import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DropdownButtonComponent} from './dropdown-button.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';

@NgModule({
    imports: [CommonModule, AngularFontAwesomeModule],
    declarations: [DropdownButtonComponent],
    exports: [DropdownButtonComponent]
})
export class DropdownButtonModule {}
