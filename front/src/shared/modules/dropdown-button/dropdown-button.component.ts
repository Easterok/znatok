import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
    selector: 'pdd-dropdown-button',
    templateUrl: './dropdown-button.component.html',
    styleUrls: ['./dropdown-button.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropdownButtonComponent {
    @Input() name: string;

    @Input() open = false;

    get openContent(): boolean {
        return this.open;
    }

    onToggleContent() {
        this.open = !this.open;
    }
}
