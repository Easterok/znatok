import {AbstractControl, FormGroup} from '@angular/forms';

const emailRegExp = /^[-\w]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/;
const usernameStartLetter = new RegExp('^[a-zA-Zа-яА-Я]');

export const usernameRegExp = '^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я0-9-_+@\\.]{1,20}$';
export const passwordRegExp = '^(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[a-zа-я]).*$';

export function emailValidator(control: AbstractControl): any {
    return emailRegExp.test(control.value)
        ? {}
        : {email: 'Почта должна быть в правильном формате (к примеру exapmle@example.com)'};
}

// TODO: create new component for show invalid value in fields
export function getErrors(errors: any): string {
    if (!errors) {
        return;
    }

    if (errors['required']) {
        return 'Поле обязательно для заполнения';
    }

    if (errors['minlength']) {
        return `Минимальная длина — ${errors['minlength']['requiredLength']}`;
    }

    if (errors['maxlength']) {
        return `Максимальная длина — ${errors['maxlength']['requiredLength']}`;
    }

    if (errors['email']) {
        return errors['email'];
    }

    if (errors['pattern']) {
        const pattern = errors.pattern.requiredPattern;
        const value = errors.pattern.actualValue;

        switch (pattern) {
            case usernameRegExp:
                return usernameStartLetter.test(value)
                    ? 'Имя пользователя должно состоять из букв, цифр и символов @-+_.'
                    : 'Имя пользователя должно начинаться с буквы';
            case passwordRegExp:
                return 'Пароль должен содержать цифру и заглавную букву';
        }
    }

    if (errors['mismatchedPasswords']) {
        return 'Пароли не совпадают';
    }

    if (errors['type']) {
        return errors['type'];
    }
}

export function matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {[key: string]: any} => {
        const password = group.controls[passwordKey];
        const confirmPassword = group.controls[confirmPasswordKey];

        if (password.value !== confirmPassword.value) {
            return {mismatchedPasswords: true};
        }
    };
}
