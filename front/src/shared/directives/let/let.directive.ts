import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
    selector: '[Let]'
})
export class LetDirective {
    @Input()
    set Let(context: any) {
        this.context.$implicit = this.context.Let = context;
        this.updateView();
    }

    context: any = {};

    constructor(private readonly vcRef: ViewContainerRef, private readonly templateRef: TemplateRef<any>) {}

    updateView() {
        this.vcRef.clear();
        this.vcRef.createEmbeddedView(this.templateRef, this.context);
    }
}
