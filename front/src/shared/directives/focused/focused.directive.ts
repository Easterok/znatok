import {Directive, HostBinding, Input} from '@angular/core';

@Directive({
  selector: '[pddFocused]'
})
export class FocusedDirective {
    @Input('pddFocused') focusable = true;

    @HostBinding('attr.tabindex')
    get tabIndex(): number {
        return this.focusable ? 0 : -1;
    }
}
