import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FocusedDirective} from './focused.directive';

@NgModule({
  declarations: [FocusedDirective],
  imports: [
    CommonModule
  ],
    exports: [FocusedDirective]
})
export class FocusedModule { }
