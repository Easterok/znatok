import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'russianDate'
})
export class RussianDatePipe implements PipeTransform {
    transform(value: string): any {
        const dateNow = new Date().toLocaleDateString('ru');

        if (dateNow === value) {
            return 'сегодня';
        }

        return value;
    }
}
