import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RussianDatePipe} from './russian-date.pipe';

@NgModule({
    imports: [CommonModule],
    declarations: [RussianDatePipe],
    exports: [RussianDatePipe]
})
export class RussianDateModule {}
