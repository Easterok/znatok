import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AuthService} from '@services/auth.service';
import {Category, IPddBlocksState, IState, PddBlocks} from '@store/models/state';
import {ActivatedRoute, NavigationEnd, Params, Router} from '@angular/router';
import {filter, map, take} from 'rxjs/operators';
import {supportUrls} from '@modules/app/modules/main-block/components/main-block/main-block.component';
import {LoadState} from '@store/actions/pdd-blocks.actions';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
    constructor(
        private readonly authService: AuthService,
        private readonly store: Store<IState>,
        private readonly router: Router,
        private readonly activatedRoute: ActivatedRoute
    ) {
        this.authService.init$().subscribe();
    }

    ngOnInit() {
        this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                take(1),
                map(({url}: NavigationEnd) => (url.match(/^\/\w+(\/\d+)?/) || '/')[0]),
                map((url: string) => {
                    const {queryParams} = this.activatedRoute.snapshot;

                    return {
                        url,
                        queryParams
                    };
                })
            )
            .subscribe(({url, queryParams}) => {
                this.store.dispatch(new LoadState(this.getState(url, queryParams)));
            });
    }

    private getState(fullUrl: string, queryParams: Params): IPddBlocksState {
        const [_, url, subUrl] = fullUrl.split('/');

        const isSupportUrl = supportUrls.includes('/' + url);
        const activeBlock = isSupportUrl ? (url as PddBlocks) : PddBlocks.TICKETS;

        return {
            activeBlock,
            ticket: this.getTicket(subUrl),
            question: this.getQuestion(queryParams),
            category: this.getCategory(queryParams)
        };
    }

    private getTicket(ticket: string): number | null {
        return this.parser(ticket);
    }

    private getQuestion({question}: Params): number | null {
        return this.parser(question);
    }

    private getCategory({category}: Params): number | null {
        const categoryNum = this.parser(category);

        return categoryNum === 1 || categoryNum === 0 ? categoryNum : Category.ABM;
    }

    private parser(queryValue: string): number | null {
        try {
            return queryValue ? parseInt(queryValue, 10) : null;
        } catch {
            return null;
        }
    }
}
