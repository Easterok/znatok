import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigationComponent {
    open = false;

    onToggleMenu() {
        this.open = !this.open;
    }
}
