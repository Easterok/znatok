import {Routes} from '@angular/router';
import {AuthGuard} from '../../guard/auth.guard';
import {NotFoundComponent} from '@shared/modules/not-found/not-found.component';

export const appRoutes: Routes = [
    {
        path: 'profile',
        canActivate: [AuthGuard],
        loadChildren: '../profile/profile.module#ProfileModule'
    },
    {
        path: '**',
        component: NotFoundComponent
    }
];
