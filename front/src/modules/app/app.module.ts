import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {registerLocaleData} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import localeRu from '@angular/common/locales/ru';
import {HttpClientModule, HttpClientXsrfModule} from '@angular/common/http';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {AppComponent} from '@modules/app/components/app/app.component';
import {NavigationComponent} from '@modules/app/components/navigation/navigation.component';
import {FooterComponent} from '@modules/app/components/footer/footer.component';
import {MainBlockModule} from '@modules/app/modules/main-block/main-block.module';
import {TopBarModule} from '@modules/app/modules/top-bar/top-bar.module';
import {AppRoutingModule} from '@modules/app/appRouting.module';
import {ReviewsModule} from '@modules/app/modules/reviews/reviews.module';
import {metaReducers, reducers} from '@store/reducers';
import {PddBlocksEffects} from '@store/effects/pdd-blocks.effects';
import {NotFoundComponent} from '@shared/modules/not-found/not-found.component';
import {environment} from '../../environments/environment';
import {OnlineGameAcceptComponent} from '@modules/app/modules/online-game/components/online-game-accept/online-game-accept.component';
import {MatSnackBarModule} from '@angular/material';
import {OnlineGameEffects} from '@store/effects/online-game.effects';
import {FocusedModule} from '@shared/directives/focused/focused.module';

registerLocaleData(localeRu);

@NgModule({
    declarations: [AppComponent, NavigationComponent, FooterComponent, NotFoundComponent],
    imports: [
        BrowserModule,
        MainBlockModule,
        TopBarModule,
        RouterModule,
        AppRoutingModule,
        ReviewsModule,
        BrowserAnimationsModule,
        StoreModule.forRoot(reducers, {metaReducers}),
        StoreDevtoolsModule.instrument({
            maxAge: 25,
            logOnly: environment.production
        }),
        MatSnackBarModule,
        EffectsModule.forRoot([PddBlocksEffects, OnlineGameEffects]),
        HttpClientModule,
        HttpClientXsrfModule.withOptions({
            cookieName: 'csrftoken',
            headerName: 'X-CSRFToken'
        }),
        FocusedModule,
    ],
    providers: [{provide: LOCALE_ID, useValue: 'ru'}],
    entryComponents: [OnlineGameAcceptComponent],
    bootstrap: [AppComponent]
})
export class AppModule {}
