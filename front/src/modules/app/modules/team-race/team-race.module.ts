import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TeamRaceComponent} from './team-race.component';
import {QuestionBlockModule} from '../../../../shared/modules/question-block/question-block.module';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [CommonModule, QuestionBlockModule, ReactiveFormsModule],
    declarations: [TeamRaceComponent],
    exports: [TeamRaceComponent]
})
export class TeamRaceModule {}
