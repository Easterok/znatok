import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-team-race',
    templateUrl: './team-race.component.html',
    styleUrls: ['./team-race.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeamRaceComponent {
    readonly teams = ['green', 'yellow', 'red'];
    choiceTeam;

    onChoiceTeam(team: string) {
        this.choiceTeam = this.choiceTeam === team ? null : team;
    }

    submitTeam() {
        if (!this.choiceTeam) {
            return;
        }
    }
}
