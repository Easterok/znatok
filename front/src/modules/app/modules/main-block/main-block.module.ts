import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainBlockComponent} from './components/main-block/main-block.component';
import {LeadersModule} from './modules/leaders/leaders.module';
import {MainBlockRoutingModule} from './mainBlockRouting.module';
import {TicketsModule} from './modules/tickets/tickets.module';
import {MarathonModule} from './modules/marathon/marathon.module';
import {ThemeModule} from './modules/theme/theme.module';
import {TheoryModule} from './modules/theory/theory.module';
import {ExamModule} from './modules/exam/exam.module';
import {MistakesModule} from './modules/mistakes/mistakes.module';
import {ReactiveFormsModule} from '@angular/forms';
import {TeamRaceModule} from '../team-race/team-race.module';
import {OnlineGameModule} from '../online-game/online-game.module';
import {AdvertisingModule} from '../advertising/advertising.module';
import {FocusedModule} from '@shared/directives/focused/focused.module';

@NgModule({
    imports: [
        CommonModule,
        OnlineGameModule,
        TeamRaceModule,
        AdvertisingModule,
        TheoryModule,
        TicketsModule,
        LeadersModule,
        ExamModule,
        ThemeModule,
        MarathonModule,
        MistakesModule,
        MainBlockRoutingModule,
        ReactiveFormsModule,
        FocusedModule
    ],
    declarations: [MainBlockComponent],
    exports: [MainBlockComponent]
})
export class MainBlockModule {}
