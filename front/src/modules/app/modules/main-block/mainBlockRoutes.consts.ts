import {Routes} from '@angular/router';
import {MainBlockComponent} from './components/main-block/main-block.component';
import {TicketsComponent} from './modules/tickets/components/tickets/tickets.component';
import {ExamComponent} from './modules/exam/exam.component';
import {ThemeComponent} from './modules/theme/components/theme/theme.component';
import {MarathonComponent} from './modules/marathon/marathon.component';
import {MistakesComponent} from './modules/mistakes/components/mistakes/mistakes.component';
import {TheoryComponent} from './modules/theory/components/theory/theory.component';
import {ThemeListComponent} from './modules/theme/components/theme-list/theme-list.component';
import {TheoryListComponent} from './modules/theory/components/theory-list/theory-list.component';
import {TheoryPreviewComponent} from './modules/theory/components/theory-preview/theory-preview.component';
import {TicketPreviewComponent} from './modules/tickets/components/ticket-preview/ticket-preview.component';
import {MistakePreviewComponent} from './modules/mistakes/components/mistake-preview/mistake-preview.component';
import {MistakesListComponent} from './modules/mistakes/components/mistakes-list/mistakes-list.component';
import {ThemePreviewComponent} from './modules/theme/components/theme-preview/theme-preview.component';

export const mainBlockRoutes: Routes = [
    {
        path: '',
        component: MainBlockComponent,
        children: [
            {
                path: 'tickets',
                children: [
                    {
                        path: ':id',
                        component: TicketPreviewComponent
                    },
                    {
                        path: '',
                        pathMatch: 'full',
                        component: TicketsComponent
                    }
                ]
            },
            {
                path: 'exam',
                component: ExamComponent
            },
            {
                path: 'theme',
                component: ThemeComponent,
                children: [
                    {
                        path: 'list',
                        component: ThemeListComponent
                    },
                    {
                        path: ':id',
                        component: ThemePreviewComponent
                    },
                    {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: 'list'
                    }
                ]
            },
            {
                path: 'marathon',
                component: MarathonComponent
            },
            {
                path: 'mistakes',
                component: MistakesComponent,
                children: [
                    {
                        path: ':id',
                        component: MistakePreviewComponent
                    },
                    {
                        path: '',
                        pathMatch: 'full',
                        component: MistakesListComponent
                    }
                ]
            },
            {
                path: 'theory',
                component: TheoryComponent,
                children: [
                    {
                        path: 'list',
                        component: TheoryListComponent
                    },
                    {
                        path: ':id',
                        component: TheoryPreviewComponent
                    },
                    {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: 'list'
                    }
                ]
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'tickets'
            }
        ]
    }
];
