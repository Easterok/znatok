import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {mainBlockRoutes} from './mainBlockRoutes.consts';

@NgModule({
    imports: [CommonModule, RouterModule.forChild(mainBlockRoutes)],
    declarations: [],
    exports: [RouterModule]
})
export class MainBlockRoutingModule {}
