import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {Subject} from 'rxjs/Rx';
import {takeUntil} from 'rxjs/operators';
import {Category, IState, PddBlocks} from '@store/models/state';
import {blocksSelectors} from '@store/selectors/pdd-blocks.selectors';
import {ChangeActiveBlock, SetCategory} from '@store/actions/pdd-blocks.actions';

export interface ILink {
    url: string;
    content: string;
    type: PddBlocks;
}

const links: ILink[] = [
    {
        url: '/theory',
        content: 'Теория',
        type: PddBlocks.THEORY
    },
    {
        url: '/tickets',
        content: 'Билеты',
        type: PddBlocks.TICKETS
    },
    {
        url: '/exam',
        content: 'Экзамен',
        type: PddBlocks.EXAM
    },
    {
        url: '/theme',
        content: 'По темам',
        type: PddBlocks.THEMES
    },
    {
        url: '/marathon',
        content: 'Марафон',
        type: PddBlocks.MARATHON
    },
    {
        url: '/mistakes',
        content: 'Ошибки',
        type: PddBlocks.MISTAKES
    }
];

export const supportUrls = links.map(({url}) => url);

interface ICategory {
    value: Category;
    content: string;
}

const categories: ICategory[] = [
    {
        value: Category.ABM,
        content: 'A, B, M'
    },
    {
        value: Category.CD,
        content: 'C, D'
    }
];
@Component({
    selector: 'pdd-main-block',
    templateUrl: './main-block.component.html',
    styleUrls: ['./main-block.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainBlockComponent implements OnInit, OnDestroy {
    links: ILink[] = links;
    categories: ICategory[] = categories;

    category = new FormControl();

    private destroy$ = new Subject<void>();

    constructor(private readonly store: Store<IState>) {}

    ngOnInit() {
        this.store
            .pipe(
                select(blocksSelectors.fullState),
                takeUntil(this.destroy$)
            )
            .subscribe(({category}) => {
                this.category.setValue(category, {emitEvent: false});
            });

        this.category.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(category => {
            this.store.dispatch(new SetCategory(category));
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onChangeUrl(link: ILink) {
        this.store.dispatch(new ChangeActiveBlock(link));
    }
}
