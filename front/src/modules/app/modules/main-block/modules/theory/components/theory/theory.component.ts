import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-theory',
    templateUrl: './theory.component.html',
    styleUrls: ['./theory.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TheoryComponent {}
