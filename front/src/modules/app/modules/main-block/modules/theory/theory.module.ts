import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TheoryComponent} from './components/theory/theory.component';
import {QuestionBlockModule} from '../../../../../../shared/modules/question-block/question-block.module';
import {TheoryListComponent} from './components/theory-list/theory-list.component';
import {RouterModule} from '@angular/router';
import {TheoryPreviewComponent} from './components/theory-preview/theory-preview.component';

@NgModule({
    imports: [CommonModule, QuestionBlockModule, RouterModule],
    declarations: [TheoryComponent, TheoryListComponent, TheoryPreviewComponent],
    exports: [TheoryComponent]
})
export class TheoryModule {}
