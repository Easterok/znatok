import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-theory-list',
    templateUrl: './theory-list.component.html',
    styleUrls: ['./theory-list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TheoryListComponent {
    themes = Array(24);
}
