import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-theory-preview',
    templateUrl: './theory-preview.component.html',
    styleUrls: ['./theory-preview.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TheoryPreviewComponent {}
