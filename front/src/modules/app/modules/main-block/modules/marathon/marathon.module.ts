import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MarathonComponent} from './marathon.component';
import {QuestionBlockModule} from '../../../../../../shared/modules/question-block/question-block.module';

@NgModule({
    imports: [CommonModule, QuestionBlockModule],
    declarations: [MarathonComponent],
    exports: [MarathonComponent]
})
export class MarathonModule {}
