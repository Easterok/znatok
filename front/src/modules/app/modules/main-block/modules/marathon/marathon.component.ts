import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-marathon',
    templateUrl: './marathon.component.html',
    styleUrls: ['./marathon.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MarathonComponent {
    buttons = Array(16);
    active: number;

    onClickButton(index: number) {
        this.active = index;
    }
}
