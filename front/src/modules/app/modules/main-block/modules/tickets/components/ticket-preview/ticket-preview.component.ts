import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {IQuestion, ITicket} from '@models/ticket.model';
import {select, Store} from '@ngrx/store';
import {IState} from '@store/models/state';
import {blocksSelectors} from '@store/selectors/pdd-blocks.selectors';
import {filter, map, switchMap, takeUntil} from 'rxjs/operators';
import {TicketService} from '../../services/ticket.service';
import {BehaviorSubject, Subject} from 'rxjs';
import {ChoiceQuestion} from '@store/actions/pdd-blocks.actions';
import {ChangeQuestionType, IAnswer} from '@shared/modules/question-preview/question-preview.component';

@Component({
    selector: 'pdd-ticket-preview',
    templateUrl: './ticket-preview.component.html',
    styleUrls: ['./ticket-preview.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TicketPreviewComponent implements OnInit, OnDestroy {
    showLoader$ = new BehaviorSubject<boolean>(true);
    information =
        'В Профайле в разделе "Настроки" ' +
        'Вы можете включить режим "Двойного нажатия", ' +
        'чтобы избежать случайных нажатий на ответ';
    currentQuestion = 1;
    question: IQuestion = <any>{};
    questions: IQuestion[] = [];

    private readonly destroy$ = new Subject<void>();

    constructor(
        private readonly store: Store<IState>,
        private readonly ticketService: TicketService,
        private readonly cd: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.store
            .pipe(
                select(blocksSelectors.ticket),
                filter(ticket => ticket !== null),
                switchMap(ticket => this.ticketService.getTicket$(ticket)),
                map(({questions}: ITicket) => questions),
                takeUntil(this.destroy$)
            )
            .subscribe((questions: IQuestion[]) => {
                this.questions = questions;

                this.question = this.questions[this.currentQuestion - 1];

                this.showLoader$.next(false);
            });

        this.store
            .pipe(
                select(blocksSelectors.question),
                filter(question => question !== null),
                takeUntil(this.destroy$)
            )
            .subscribe(question => {
                this.currentQuestion = question;

                this.question = this.questions[question - 1];

                this.cd.markForCheck();
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
    }

    changeQuestion(question: ChangeQuestionType) {
        const questionNumber = typeof question === 'number' ? question : this.getNextNumber(question);

        this.store.dispatch(new ChoiceQuestion(questionNumber));
    }

    getNextNumber(question: ChangeQuestionType): number {
        const nextStep = this.currentQuestion + 1;
        const previousStep = this.currentQuestion - 1;

        switch (question) {
            case 'previous':
                return previousStep < 1 ? this.currentQuestion : previousStep;
            case 'next':
                return nextStep > this.questions.length ? this.currentQuestion : nextStep;
        }
    }

    checkAnswer(answer: IAnswer) {
        this.showLoader$.next(true);
        this.ticketService.checkAnswer(answer).subscribe(
            response => {
                this.question = {...this.question, result: response};
                this.questions = [
                    ...this.questions.slice(0, this.currentQuestion - 1),
                    this.question,
                    ...this.questions.slice(this.currentQuestion)
                ];

                this.showLoader$.next(false);
            },
            () => this.showLoader$.next(false)
        );
    }
}
