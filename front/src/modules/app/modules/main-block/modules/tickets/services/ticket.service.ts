import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs/Rx';
import {IQuestion, IResultQuestion, ITicket} from '../../../../../../../models/ticket.model';
import {IAnswer} from '../../../../../../../shared/modules/question-preview/question-preview.component';
import {map, tap} from 'rxjs/operators';
import {UserStateService} from '../../../../../../../services/user-state.service';
import {Category} from '@store/models/state';

@Injectable({
    providedIn: 'root'
})
export class TicketService {
    private tickets$ = new BehaviorSubject<ITicket[]>(new Array(40));

    constructor(private readonly http: HttpClient, private userStateService: UserStateService) {}

    get _tickets$(): Observable<ITicket[]> {
        return this.tickets$.asObservable();
    }

    getTickets$(category: Category): Observable<ITicket[]> {
        const url = `/api/tickets/${category}/`;

        return this.http.get<ITicket[]>(url, {withCredentials: true}).pipe(
            map(tickets => {
                return tickets.map(({id, number, ticket_result}) => {
                    return {
                        id,
                        number,
                        done: ticket_result['true'],
                        error: ticket_result['false']
                    };
                });
            }),
            tap(tickets => this.tickets$.next(tickets))
        );
    }

    getTicket$(id: number): Observable<IQuestion[]> {
        const url = '/api/ticket/' + id + '/';

        return this.http.get<IQuestion[]>(url, {withCredentials: true});
    }

    checkAnswer(answer: IAnswer): Observable<IResultQuestion> {
        const url = '/api/question/';

        return this.http.post<IResultQuestion>(url, answer, {withCredentials: true}).pipe(
            tap(response => {
                response.is_true ? this.userStateService.incPoints() : null;
            })
        );
    }
}
