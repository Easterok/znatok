import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TicketsComponent} from './components/tickets/tickets.component';
import {QuestionBlockModule} from '../../../../../../shared/modules/question-block/question-block.module';
import {ProgressBarModule} from '../../../../../../shared/modules/progress-bar/progress-bar.module';
import {QuestionPreviewModule} from '../../../../../../shared/modules/question-preview/question-preview.module';
import {ButtonListModule} from '../../../../../../shared/modules/button-list/button-list.module';
import {TicketPreviewComponent} from './components/ticket-preview/ticket-preview.component';
import {LoaderModule} from '../../../../../../shared/modules/loader/loader.module';
import {FocusedModule} from '@shared/directives/focused/focused.module';

@NgModule({
    imports: [
        CommonModule,
        QuestionBlockModule,
        QuestionPreviewModule,
        ButtonListModule,
        ProgressBarModule,
        RouterModule,
        LoaderModule,
        FocusedModule
    ],
    declarations: [TicketsComponent, TicketPreviewComponent],
    exports: [TicketsComponent]
})
export class TicketsModule {}
