import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {ITicket} from '@models/ticket.model';
import {TicketService} from '../../services/ticket.service';
import {select, Store} from '@ngrx/store';
import {IState} from '@store/models/state';
import {blocksSelectors} from '@store/selectors/pdd-blocks.selectors';
import {switchMap, takeUntil, tap} from 'rxjs/operators';
import {ChoiceTicket} from '@store/actions/pdd-blocks.actions';
import {UserStateService} from '@services/user-state.service';
import {withLatestFrom} from 'rxjs/operators';

@Component({
    selector: 'pdd-tickets',
    templateUrl: './tickets.component.html',
    styleUrls: ['./tickets.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TicketsComponent implements OnInit, OnDestroy {
    showLoader$ = new BehaviorSubject<boolean>(true);
    tickets: Observable<ITicket[]>;

    private readonly destroy$ = new Subject<void>();

    constructor(
        private readonly ticketService: TicketService,
        private readonly store: Store<IState>,
        private readonly uss: UserStateService
    ) {}

    ngOnInit() {
        this.tickets = this.ticketService._tickets$;

        this.uss._user$
            .pipe(
                withLatestFrom(this.store.pipe(select(blocksSelectors.category))),
                switchMap(([_, category]) => this.ticketService.getTickets$(category)),
                takeUntil(this.destroy$)
            )
            .subscribe();

        this.store
            .pipe(
                select(blocksSelectors.category),
                switchMap(category => this.ticketService.getTickets$(category)),
                tap(() => this.showLoader$.next(false)),
                takeUntil(this.destroy$)
            )
            .subscribe();
    }

    ngOnDestroy() {
        this.destroy$.next();
    }

    onSelectTicket(ticketId: number) {
        this.store.dispatch(new ChoiceTicket(ticketId));
    }
}
