import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-mistakes',
    templateUrl: './mistakes.component.html',
    styleUrls: ['./mistakes.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MistakesComponent {}
