import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-mistakes-list',
    templateUrl: './mistakes-list.component.html',
    styleUrls: ['./mistakes-list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MistakesListComponent {
    mistakes = Array(5);
}
