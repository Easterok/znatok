import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MistakesComponent} from './components/mistakes/mistakes.component';
import {MistakePreviewComponent} from './components/mistake-preview/mistake-preview.component';
import {MistakesListComponent} from './components/mistakes-list/mistakes-list.component';
import {QuestionBlockModule} from '../../../../../../shared/modules/question-block/question-block.module';
import {RouterModule} from '@angular/router';
import {QuestionPreviewModule} from '../../../../../../shared/modules/question-preview/question-preview.module';
import {PaginationModule} from '../../../../../../shared/modules/pagination/pagination.module';

@NgModule({
    imports: [CommonModule, QuestionBlockModule, QuestionPreviewModule, PaginationModule, RouterModule],
    declarations: [MistakesComponent, MistakePreviewComponent, MistakesListComponent],
    exports: [MistakesComponent]
})
export class MistakesModule {}
