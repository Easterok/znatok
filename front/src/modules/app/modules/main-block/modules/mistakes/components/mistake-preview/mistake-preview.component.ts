import {Component} from '@angular/core';

@Component({
    selector: 'pdd-mistake-preview',
    templateUrl: './mistake-preview.component.html',
    styleUrls: ['./mistake-preview.component.less']
})
export class MistakePreviewComponent {
    tickets = {
        button: Array(5)
    };
}
