import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-exam',
    templateUrl: './exam.component.html',
    styleUrls: ['./exam.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExamComponent {
    readonly information = `Пройдя экзамен, вам так же начислятся быллы за верные ответы.
    Изображения будут идентичны изображениям соответствующих вопросов экзаменнационных билетов,
    утвержденных ГУГИБДД России.`;

    startExam() {
        console.log(' exam start ---->');
    }
}
