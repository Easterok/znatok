import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExamComponent} from './exam.component';
import {QuestionBlockModule} from '../../../../../../shared/modules/question-block/question-block.module';

@NgModule({
    imports: [CommonModule, QuestionBlockModule],
    declarations: [ExamComponent],
    exports: [ExamComponent]
})
export class ExamModule {}
