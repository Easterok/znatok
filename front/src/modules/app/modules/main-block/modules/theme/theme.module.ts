import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ThemeComponent} from './components/theme/theme.component';
import {ThemeListComponent} from './components/theme-list/theme-list.component';
import {ThemePreviewComponent} from './components/theme-preview/theme-preview.component';
import {RouterModule} from '@angular/router';
import {QuestionBlockModule} from '../../../../../../shared/modules/question-block/question-block.module';
import {QuestionPreviewModule} from '../../../../../../shared/modules/question-preview/question-preview.module';
import {ButtonListModule} from '../../../../../../shared/modules/button-list/button-list.module';

@NgModule({
    imports: [CommonModule, RouterModule, QuestionBlockModule, QuestionPreviewModule, ButtonListModule, RouterModule],
    declarations: [ThemeComponent, ThemeListComponent, ThemePreviewComponent],
    exports: [ThemeComponent]
})
export class ThemeModule {}
