import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-theme-list',
    templateUrl: './theme-list.component.html',
    styleUrls: ['./theme-list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThemeListComponent {
    themes = Array(10);
}
