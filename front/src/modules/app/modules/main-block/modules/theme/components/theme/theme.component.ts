import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-theme',
    templateUrl: './theme.component.html',
    styleUrls: ['./theme.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThemeComponent {
    readonly information = `Отвечая на вопросы по темам на результат в 100%, 
    в личном кабинете (блок достижения) появятся похвальные грамоты по пройденным темам. 
    так же, в процессе прохождения, вам будут начисляться баллы за верные ответы`;
}
