import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-theme-preview',
    templateUrl: './theme-preview.component.html',
    styleUrls: ['./theme-preview.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThemePreviewComponent {
    tickets = {
        button: Array(5)
    };
}
