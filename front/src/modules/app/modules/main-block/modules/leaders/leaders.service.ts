import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs/Rx';

@Injectable({
    providedIn: 'root'
})
export class LeadersService {
    private leaders$ = new BehaviorSubject<any>(null);

    set leaders(leaders: any) {
        this.leaders$.next(leaders);
    }

    get _leaders$(): Observable<any> {
        return this.leaders$.asObservable();
    }
}
