import {ChangeDetectionStrategy, Component} from '@angular/core';
import {LeadersService} from './leaders.service';
import {Observable} from 'rxjs/Rx';

@Component({
    selector: 'pdd-leaders',
    templateUrl: './leaders.component.html',
    styleUrls: ['./leaders.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LeadersComponent {
    leaders$: Observable<any>;

    constructor(private leadersService: LeadersService) {
        this.leaders$ = this.leadersService._leaders$;
    }
}
