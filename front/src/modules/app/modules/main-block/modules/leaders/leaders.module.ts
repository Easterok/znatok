import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LeadersComponent} from './leaders.component';

@NgModule({
    imports: [CommonModule],
    declarations: [LeadersComponent],
    exports: [LeadersComponent]
})
export class LeadersModule {}
