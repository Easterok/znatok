import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {OnlineGameService} from '../../servises/online-game.service';
import {Observable} from 'rxjs/Rx';
import {UserStateService} from '@services/user-state.service';
import {IStartOnlineGame} from '../online-game-default/online-game-default.component';
import {IUser} from '@models/new-user.model';
import {tap} from 'rxjs/operators';
import {OnlineGameEnum, resultStatusesTypes} from '@store/reducers/online-game.reducer';
import {IState} from '@store/models/state';
import {select, Store} from '@ngrx/store';
import {SetGameId, SetProcess, SetQuestions} from '@store/actions/online-game.actions';
import {onlineGameSelectors} from '@store/selectors/online-game.selectors';
import {IQuestion} from '@models/ticket.model';
import {IResultGame} from '@modules/app/modules/online-game/components/online-game-result/online-game-result.component';

@Component({
    selector: 'pdd-online-game',
    templateUrl: './online-game.component.html',
    styleUrls: ['./online-game.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OnlineGameComponent {
    readonly onlineGame = OnlineGameEnum;

    readonly process$: Observable<OnlineGameEnum>;

    readonly gameId$: Observable<number>;

    readonly questions$: Observable<IQuestion[]>;

    result: IResultGame = {
        status: OnlineGameEnum.WAITING_ENEMY,
        score: null,
        enemyScore: null,
        wrongScore: null,
        enemyWrongScore: null
    };

    points: number = 0;

    user$: Observable<any>;

    user: IUser;

    constructor(
        private readonly onlineGameService: OnlineGameService,
        private readonly userStateService: UserStateService,
        private readonly store: Store<IState>,
        private readonly cd: ChangeDetectorRef
    ) {
        this.process$ = this.store.pipe(select(onlineGameSelectors.process));

        this.gameId$ = this.store.pipe(select(onlineGameSelectors.gameId));

        this.questions$ = this.store.pipe(select(onlineGameSelectors.questions));

        this.user$ = this.userStateService._user$.pipe(
            tap(user => {
                this.user = user;
            })
        );
    }

    isEnd(status: OnlineGameEnum): boolean {
        return resultStatusesTypes.includes(status) || status === OnlineGameEnum.WAITING_ENEMY;
    }

    startGame(onlineGame: IStartOnlineGame) {
        let questionsLength: number = 0;
        this.points = onlineGame.bet;
        this.onlineGameService.socketConnect(this.user).subscribe(({status, gameId, questions, enemy_score, score}) => {
            this.store.dispatch(new SetProcess(status));

            if (gameId) {
                this.store.dispatch(new SetGameId(gameId));
            }

            if (questions) {
                questionsLength = questions.length;

                const correctQuestions = questions.map(({result, ...rest}) => {
                    return {...rest};
                });

                this.store.dispatch(new SetQuestions(correctQuestions));
            }

            if (resultStatusesTypes.includes(status)) {
                this.result = {
                    status,
                    score,
                    wrongScore: questionsLength - score,
                    enemyScore: enemy_score === undefined ? score : enemy_score,
                    enemyWrongScore: enemy_score === undefined ? questionsLength - score : questionsLength - enemy_score
                };

                this.cd.markForCheck();
            }
        });

        this.onlineGameService.createGame(onlineGame).subscribe();
    }
}
