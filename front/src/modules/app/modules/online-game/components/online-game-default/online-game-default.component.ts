import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    Output
} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserStateService} from '@services/user-state.service';
import {IState} from '@store/models/state';
import {select, Store} from '@ngrx/store';
import {onlineGameSelectors} from '@store/selectors/online-game.selectors';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/internal/operators';
import {OnlineGameEnum} from '@store/reducers/online-game.reducer';

export interface IStartOnlineGame {
    bet: number;
    category: string;
}

@Component({
    selector: 'pdd-online-game-default',
    templateUrl: './online-game-default.component.html',
    styleUrls: ['./online-game-default.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OnlineGameDefaultComponent implements OnDestroy {
    @Input() user: any;

    @Input()
    set setPoints(points: number) {
        this.form.get('points').setValue(points, {emitEvent: false});
    }

    @Output() startGame = new EventEmitter<IStartOnlineGame>();

    readonly information = `Выберите ставку. Нажмите СТАРТ. Дождитесь готовности соперника.
            Побеждает тот, кто первый и без ошибок ответит на 20 вопросов из билетов ПДД.
            Баллы по ставке от проигравшего переходят победителю. Удачи!`;
    form: FormGroup;
    readonly pointsValue = [0, 50, 100, 250, 500, 1000];
    disabledIndex: number = 0;

    private readonly destroy$ = new Subject<void>();

    constructor(
        private readonly fb: FormBuilder,
        private readonly userStateService: UserStateService,
        private readonly store: Store<IState>
    ) {
        this.form = this.fb.group({
            category: ['pdd'],
            points: ['0']
        });

        this.userStateService
            .getPoints$()
            .pipe(takeUntil(this.destroy$))
            .subscribe(points => {
                this.disabledIndex = this.pointsValue.findIndex(value => value > points);
            });

        this.subscribeOnStore();
    }

    get category(): string {
        return this.form.get('category').value;
    }

    get points(): number {
        return this.form.get('points').value;
    }

    ngOnDestroy() {
        this.destroy$.next();
    }

    startOnlineGame() {
        if (this.form.get('category').disabled) {
            return;
        }

        this.startGame.emit({
            bet: this.points,
            category: this.category
        });
    }

    private subscribeOnStore() {
        this.store
            .pipe(
                select(onlineGameSelectors.process),
                takeUntil(this.destroy$)
            )
            .subscribe(process => {
                if (process === OnlineGameEnum.WAITING) {
                    this.form.get('category').disable({emitEvent: false});
                }

                if (process === OnlineGameEnum.DEFAULT) {
                    this.form.get('category').enable({emitEvent: false});
                }
            });
    }
}
