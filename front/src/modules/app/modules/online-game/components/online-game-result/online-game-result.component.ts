import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {OnlineGameEnum} from '@store/reducers/online-game.reducer';

export interface IResultGame {
    status: OnlineGameEnum;
    score: number;
    wrongScore: number;
    enemyScore: number;
    enemyWrongScore: number;
}

@Component({
    selector: 'pdd-online-game-result',
    templateUrl: './online-game-result.component.html',
    styleUrls: ['./online-game-result.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OnlineGameResultComponent implements OnInit {
    @Input() resultGame: IResultGame;

    message = 'Ожидание результата...';

    time = new Date(2018, 10, 10, 10, 10, 10);

    ngOnInit() {
        const {status} = this.resultGame;

        if (this.isWon) {
            this.message = 'Вы победили, поздравляем!';
        } else if (this.isLost) {
            this.message = 'Вы проиграли';
        } else if (status === OnlineGameEnum.DRAWN) {
            this.message = 'Ничья!';
        }
    }

    get isWon(): boolean {
        return this.resultGame.status === OnlineGameEnum.WON;
    }

    get isLost(): boolean {
        return this.resultGame.status === OnlineGameEnum.LOST;
    }

    get waitingEnemy(): boolean {
        return this.resultGame.status === OnlineGameEnum.WAITING_ENEMY;
    }
}
