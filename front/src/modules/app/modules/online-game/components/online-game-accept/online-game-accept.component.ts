import {ChangeDetectionStrategy, Component} from '@angular/core';
import {MatSnackBarRef} from '@angular/material';
import {Store} from '@ngrx/store';
import {IState} from '@store/models/state';
import {SetProcess} from '@store/actions/online-game.actions';
import {OnlineGameEnum} from '@store/reducers/online-game.reducer';
import {timer} from 'rxjs';

@Component({
    selector: 'pdd-online-game-accept',
    templateUrl: './online-game-accept.component.html',
    styleUrls: ['./online-game-accept.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OnlineGameAcceptComponent {
    readonly timer = 5;
    readonly progress$ = timer(0, 500);

    constructor(
        private readonly snackBarRef: MatSnackBarRef<OnlineGameAcceptComponent>,
        private readonly store: Store<IState>
    ) {}

    submit(action: boolean) {
        const payload = action ? OnlineGameEnum.OK : OnlineGameEnum.CANCEL;

        this.store.dispatch(new SetProcess(payload));

        this.close();
    }

    close() {
        this.snackBarRef.dismiss();
    }
}
