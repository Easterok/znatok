import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {OnlineGameService} from '../../servises/online-game.service';
import {IQuestion} from '@models/ticket.model';
import {IAnswer} from '@shared/modules/question-preview/question-preview.component';

export interface IOnlineGameAnswers {
    gameId: number;
    answers: IOnlineAnswer[];
}

interface IOnlineAnswer {
    questionId: number;
    answer: string;
}

@Component({
    selector: 'pdd-online-game-process',
    templateUrl: './online-game-process.component.html',
    styleUrls: ['./online-game-process.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OnlineGameProcessComponent {
    @Input() gameId: number;

    @Input() questions: IQuestion[] = [];

    active = 0;

    private answers: IOnlineAnswer[] = [];

    constructor(private readonly onlineGameService: OnlineGameService, private readonly cd: ChangeDetectorRef) {}

    checkAnswer({id: questionId, answer}: IAnswer) {
        this.answers.push({questionId, answer});

        if (this.answers.length === this.questions.length) {
            const {gameId, answers} = this;

            this.onlineGameService.checkAnswers({gameId, answers}).subscribe();
        } else {
            this.active += 1;
            this.cd.markForCheck();
        }
    }
}
