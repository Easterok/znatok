import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OnlineGameComponent} from './components/online-game/online-game.component';
import {ReactiveFormsModule} from '@angular/forms';
import {StatisticComponent} from './statistic/statistic.component';
import {OnlineGameDefaultComponent} from './components/online-game-default/online-game-default.component';
import {OnlineGameProcessComponent} from './components/online-game-process/online-game-process.component';
import {OnlineGameResultComponent} from './components/online-game-result/online-game-result.component';
import {RouterModule} from '@angular/router';
import {OnlineGameAcceptComponent} from './components/online-game-accept/online-game-accept.component';
import {ButtonModule} from '@shared/modules/button/button.module';
import {LetModule} from '@shared/directives/let/let.module';
import {ProgressBarModule} from '@shared/modules/progress-bar/progress-bar.module';
import {QuestionBlockModule} from '@shared/modules/question-block/question-block.module';
import {QuestionPreviewModule} from '@shared/modules/question-preview/question-preview.module';
import {FocusedModule} from '@shared/directives/focused/focused.module';

@NgModule({
    declarations: [
        OnlineGameComponent,
        StatisticComponent,
        OnlineGameDefaultComponent,
        OnlineGameProcessComponent,
        OnlineGameResultComponent,
        OnlineGameAcceptComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        LetModule,
        QuestionBlockModule,
        QuestionPreviewModule,
        RouterModule,
        ButtonModule,
        ProgressBarModule,
        FocusedModule
    ],
    exports: [OnlineGameComponent]
})
export class OnlineGameModule {}
