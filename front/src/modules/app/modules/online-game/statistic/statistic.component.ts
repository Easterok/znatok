import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-statistic',
    templateUrl: './statistic.component.html',
    styleUrls: ['./statistic.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatisticComponent {}
