import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {IStartOnlineGame} from '../components/online-game-default/online-game-default.component';
import {IUser} from '@models/new-user.model';
import Centrifuge from 'centrifuge';
import {Subject} from 'rxjs';
import {convertFromDTO, convertToDTO} from '../../../../../utils/convertDTO';
import {OnlineGameEnum} from '@store/reducers/online-game.reducer';
import {MatSnackBar} from '@angular/material';
import {IState} from '@store/models/state';
import {select, Store} from '@ngrx/store';
import {onlineGameSelectors} from '@store/selectors/online-game.selectors';
import {distinctUntilChanged} from 'rxjs/operators';
import {OnlineGameAcceptComponent} from '@modules/app/modules/online-game/components/online-game-accept/online-game-accept.component';
import {IOnlineGameAnswers} from '@modules/app/modules/online-game/components/online-game-process/online-game-process.component';
import {IQuestion} from '@models/ticket.model';
import {MatSnackBarConfig} from '@angular/material/snack-bar/typings/snack-bar-config';

const snackConfig: MatSnackBarConfig = {
    verticalPosition: 'top',
    horizontalPosition: 'right',
    duration: 5000
};

@Injectable({
    providedIn: 'root'
})
export class OnlineGameService {
    constructor(
        private readonly http: HttpClient,
        private readonly store: Store<IState>,
        private readonly snackBar: MatSnackBar
    ) {
        this.subscribeOnShowSnackBar();
    }

    socketConnect(user: IUser) {
        const subject = new Subject<{
            status: OnlineGameEnum;
            gameId?: number;
            questions?: IQuestion[];
            score?: number;
            enemy_score?: number;
        }>();
        const centrifuge = new Centrifuge('ws://localhost:8088/connection/websocket');

        centrifuge.setToken(user.token);

        centrifuge.subscribe(`#${user.id}`, ({data}) => {
            subject.next(convertFromDTO(data) as any);
        });

        centrifuge.connect();

        return subject.asObservable();
    }

    createGame(onlineGame: IStartOnlineGame) {
        return this.http.post('/api/game/create/', onlineGame);
    }

    confirmGame(gameId: number): Observable<any> {
        return this.http.post('/api/game/confirm/', convertToDTO({gameId}));
    }

    checkAnswers(answers: IOnlineGameAnswers): Observable<any> {
        return this.http.post('/api/game/answer/', convertToDTO(answers));
    }

    private subscribeOnShowSnackBar() {
        this.store
            .pipe(
                select(onlineGameSelectors.process),
                distinctUntilChanged()
            )
            .subscribe(process => {
                switch (process) {
                    case OnlineGameEnum.CONFIRM:
                        this.snackBar.openFromComponent(OnlineGameAcceptComponent, snackConfig);
                        break;
                    case OnlineGameEnum.SELF_ANSWER_TIMEOUT:
                        this.snackBar.open('Вы не успели ответить на вопрос', null, snackConfig);
                        break;
                    case OnlineGameEnum.ENEMY_ANSWER_TIMEOUT:
                        this.snackBar.open('Противник не успел ответить на вопрос', null, snackConfig);
                }
            });
    }
}
