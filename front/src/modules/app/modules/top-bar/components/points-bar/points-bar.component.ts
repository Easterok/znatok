import {ChangeDetectionStrategy, Component} from '@angular/core';
import {UserStateService} from '../../../../../../services/user-state.service';
import {Observable} from 'rxjs/Rx';
import {IUser} from '../../../../../../models/new-user.model';

@Component({
    selector: 'pdd-points-bar',
    templateUrl: './points-bar.component.html',
    styleUrls: ['./points-bar.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PointsBarComponent {
    readonly points$: Observable<number>;
    readonly teamPoints$: Observable<number>;
    readonly beepcoin$: Observable<number>;
    readonly user$: Observable<IUser>;

    constructor(private readonly userStateService: UserStateService) {
        this.points$ = this.userStateService.getPoints$();
        this.teamPoints$ = this.userStateService.getTeamPoints$();
        this.beepcoin$ = this.userStateService.getBeepCoin$();
    }
}
