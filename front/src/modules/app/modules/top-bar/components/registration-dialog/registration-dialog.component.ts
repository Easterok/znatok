import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject} from '@angular/core';
import {LoginDialogComponent} from '../login-dialog/login-dialog.component';
import {
    emailValidator,
    getErrors,
    matchingPasswords,
    passwordRegExp,
    usernameRegExp
} from '../../../../../../shared/functions/formValidators';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../../../../services/auth.service';
import {INewUser} from '../../../../../../models/new-user.model';

const DEFAULT_ERROR_MESSAGE = 'Ошибка регистрации, попробуйте еще раз.';

interface IErrorRegistrationResponse {
    username: string[];
    email: string[];
    password: string[];
}

@Component({
    selector: 'pdd-registration-dialog',
    templateUrl: './registration-dialog.component.html',
    styleUrls: ['./registration-dialog.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegistrationDialogComponent {
    getErrors: Function;
    form: FormGroup;
    showAlert = false;
    showLoader = false;
    message = DEFAULT_ERROR_MESSAGE;
    isSuccessMessage = false;

    constructor(
        private readonly authService: AuthService,
        private readonly fb: FormBuilder,
        private readonly dialogRef: MatDialogRef<LoginDialogComponent>,
        private readonly cd: ChangeDetectorRef,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.form = this.fb.group(
            {
                username: [
                    '',
                    [
                        Validators.required,
                        Validators.minLength(2),
                        Validators.maxLength(150),
                        Validators.pattern(usernameRegExp)
                    ]
                ],
                email: ['', [Validators.required, emailValidator]],
                password: ['', [Validators.required, Validators.minLength(8), Validators.pattern(passwordRegExp)]],
                passwordConfirm: [
                    '',
                    [Validators.required, Validators.minLength(8), Validators.pattern(passwordRegExp)]
                ]
            },
            {validator: matchingPasswords('password', 'passwordConfirm')}
        );

        this.getErrors = getErrors;

        this.form.setValue({
            username: 'easter',
            email: 'caji@mail.ru',
            password: 'qwert12345',
            passwordConfirm: 'qwert12345'
        });
    }

    get username(): string {
        return this.form.get('username').value;
    }

    get email(): string {
        return this.form.get('email').value;
    }

    get password(): string {
        return this.form.get('password').value;
    }

    closeDialog() {
        this.dialogRef.close();
    }

    onSubmitRegistration() {
        if (this.isSuccessMessage) {
            return;
        }

        Object.keys(this.form.controls).forEach(key => {
            this.form.get(key).markAsTouched({onlySelf: true});
        });

        console.log('this.form ---->', this.form);
        if (this.form.invalid) {
            return;
        }

        const newUser = <INewUser>{
            username: this.username,
            email: this.email,
            password: this.password
        };

        this.showLoader = true;

        this.authService.registration$(newUser).subscribe(
            (response: INewUser) => {
                this.showLoader = !this.showLoader;
                this.isSuccessMessage = true;
                this.showAlert = true;
                this.changeAlertMessage();
                this.form.reset();

                this.cd.markForCheck();
            },
            ({status, error}) => {
                if (status === 401) {
                    this.changeAlertMessage(error);
                }

                this.showLoader = !this.showLoader;
                this.showAlert = true;
                this.cd.markForCheck();
            }
        );
    }

    private changeAlertMessage(error?: IErrorRegistrationResponse) {
        if (error === undefined) {
            this.message = 'Для подтверждения регистрации пройдите ' + 'по ссылке, отправленной на Вашу почту';

            return;
        }

        if (error['username'] && error['email']) {
            this.message = 'Имя пользователя и почта заняты';

            return;
        }

        if (error['username']) {
            this.message = 'Такое имя пользователя занято';

            return;
        }

        if (error['email']) {
            this.message = 'Такая почта занята';

            return;
        }
    }
}
