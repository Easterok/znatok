import {ChangeDetectionStrategy, Component, EventEmitter, Output} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {RouterService} from '@services/router.service';
import {AuthService} from '@services/auth.service';
import {UserStateService} from '@services/user-state.service';

@Component({
    selector: 'pdd-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {
    @Output() loginDialog = new EventEmitter<void>();

    @Output() registrationDialog = new EventEmitter<void>();

    user$: Observable<any>;

    constructor(
        private readonly authService: AuthService,
        private readonly userStateService: UserStateService,
        private readonly routerService: RouterService
    ) {
        this.user$ = this.userStateService._user$;
    }

    openLoginDialog() {
        this.loginDialog.emit();
    }

    openRegistrationDialog() {
        this.registrationDialog.emit();
    }

    goToHome() {
        this.routerService.setUrl(['/']);
    }

    logout() {
        this.authService.logout$().subscribe();
    }
}
