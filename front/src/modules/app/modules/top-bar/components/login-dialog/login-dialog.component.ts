import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {getErrors} from '../../../../../../shared/functions/formValidators';
import {AuthService} from '../../../../../../services/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'pdd-login-dialog',
    templateUrl: './login-dialog.component.html',
    styleUrls: ['./login-dialog.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginDialogComponent {
    form: FormGroup;
    getErrors: Function;
    showAlert = false;
    showLoader = false;
    message = 'Проблема с входом. Попробуйте еще раз.';

    constructor(
        private dialogRef: MatDialogRef<LoginDialogComponent>,
        private authService: AuthService,
        private router: Router,
        private cd: ChangeDetectorRef,
        @Inject(MAT_DIALOG_DATA) private data: any
    ) {
        this.form = new FormGroup({
            username: new FormControl('', [Validators.required, Validators.minLength(2)]),
            password: new FormControl('', [Validators.required, Validators.minLength(2)])
        });

        this.getErrors = getErrors;
    }

    get username(): AbstractControl {
        return this.form.get('username');
    }

    get password(): AbstractControl {
        return this.form.get('password');
    }

    closeDialog() {
        this.dialogRef.close();
    }

    onSubmitLogin() {
        Object.keys(this.form.controls).forEach(key => {
            this.form.get(key).markAsTouched({onlySelf: true});
        });

        if (this.form.invalid) {
            return;
        }

        const user = {
            username: this.username.value,
            password: this.password.value
        };

        this.showLoader = !this.showLoader;
        this.cd.markForCheck();

        this.authService.login$(user).subscribe(
            () => {
                this.showLoader = !this.showLoader;
                this.cd.markForCheck();
                this.closeDialog();
                this.router.navigate(['']);
            },
            error => {
                this.showError(error);
                this.showLoader = !this.showLoader;
                this.cd.markForCheck();
            }
        );
    }

    private showError(error: any) {
        this.showAlert = true;

        if (error.status === 403) {
            this.message = 'Скорее всего вы уже авторизованы.';

            return;
        }

        if (error.status === 406) {
            this.message = 'Аккаунт не активирован. ' + 'Вам необходим перейти по ссылке в письме на вашей почте.';
        }
    }
}
