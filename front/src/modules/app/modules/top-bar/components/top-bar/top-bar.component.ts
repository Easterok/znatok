import {ChangeDetectionStrategy, Component} from '@angular/core';
import {MatDialog} from '@angular/material';
import {LoginDialogComponent} from '../login-dialog/login-dialog.component';
import {RegistrationDialogComponent} from '../registration-dialog/registration-dialog.component';

@Component({
    selector: 'pdd-top-bar',
    templateUrl: './top-bar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopBarComponent {
    constructor(private dialog: MatDialog) {}

    loginDialogOpen() {
        this.dialog.open(LoginDialogComponent, {
            minHeight: '325px',
            width: '400px'
        });
    }

    registrationDialogOpen() {
        this.dialog.open(RegistrationDialogComponent, {
            width: '50%'
        });
    }
}
