import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TopBarComponent} from './components/top-bar/top-bar.component';
import {HeaderComponent} from './components/header/header.component';
import {MiniGameModule} from './modules/mini-game/mini-game.module';
import {PointsBarComponent} from './components/points-bar/points-bar.component';
import {RouterModule} from '@angular/router';
import {LoginDialogComponent} from './components/login-dialog/login-dialog.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {RegistrationDialogComponent} from './components/registration-dialog/registration-dialog.component';
import {LoaderModule} from '../../../../shared/modules/loader/loader.module';
import {FormErrorModule} from '../../../../shared/modules/form-error/form-error.module';
import {FocusedModule} from '@shared/directives/focused/focused.module';

@NgModule({
    declarations: [
        TopBarComponent,
        HeaderComponent,
        PointsBarComponent,
        LoginDialogComponent,
        RegistrationDialogComponent
    ],
    imports: [
        CommonModule,
        MiniGameModule,
        RouterModule,
        ReactiveFormsModule,
        AngularFontAwesomeModule,
        BrowserAnimationsModule,
        MatDialogModule,
        LoaderModule,
        FormErrorModule,
        FocusedModule
    ],
    entryComponents: [LoginDialogComponent, RegistrationDialogComponent],
    exports: [TopBarComponent]
})
export class TopBarModule {}
