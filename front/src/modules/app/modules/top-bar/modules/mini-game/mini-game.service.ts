import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IMiniGame, IMiniGameAnswer, IMiniGameCheckAnswer,} from '@modules/app/modules/top-bar/modules/mini-game/mini-game.models';
import {map} from 'rxjs/operators';
import {convertFromDTO} from '../../../../../../utils/convertDTO';

const MINI_GAME_URL = 'api/mini_game/';

@Injectable({
    providedIn: 'root'
})
export class MiniGameService {
    constructor(private readonly http: HttpClient) {
    }

    load(): Observable<IMiniGame> {
        return this.http.post<IMiniGame>(MINI_GAME_URL + 'load/', null, {withCredentials: true})
            .pipe(map(convertFromDTO));
    }

    checkAnswer(answer: IMiniGameAnswer): Observable<IMiniGameCheckAnswer> {
        return this.http.post<IMiniGameCheckAnswer>(MINI_GAME_URL + 'check_answer/', answer, {withCredentials: true})
            .pipe(map(convertFromDTO));
    }
}
