import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {BehaviorSubject, merge, timer} from 'rxjs';
import {distinctUntilChanged, map, switchMap, tap} from 'rxjs/operators';
import {UserStateService} from '@services/user-state.service';
import {MiniGameService} from '@modules/app/modules/top-bar/modules/mini-game/mini-game.service';
import {IMiniGame, IMiniGameAnswer} from '@modules/app/modules/top-bar/modules/mini-game/mini-game.models';
import {mockSigns} from '@modules/app/modules/top-bar/modules/mini-game/mockSigns';

const SHOW_RESULT_TIME = 1000;

@Component({
    selector: 'pdd-mini-game',
    templateUrl: './mini-game.component.html',
    styleUrls: ['./mini-game.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MiniGameComponent {
    right: boolean = null;

    game: IMiniGame = {
        attemptNumber: 5,
        question: 'Выберите знаки',
        signs: mockSigns,
        ready: true
    };

    timer: Date = null;

    readonly showLoader$ = new BehaviorSubject<boolean>(false);

    private selectedSigns: number[] = [];

    private readonly init$ = new BehaviorSubject<void>(null);

    readonly realTime$ = timer(0, 1000).pipe(
        map(() => new Date()),
        tap(date => {
            const diff = new Date(<any>this.timer - <any>date);

            if (diff.getMinutes() === 0 && diff.getSeconds() === 0) {
                this.init$.next(null);
            }
        })
    );

    constructor(private readonly cd: ChangeDetectorRef,
                private readonly miniGameService: MiniGameService,
                private readonly userStateService: UserStateService) {
        this.subscribeOnInitGame();
    }

    onClick({id}) {
        if (this.isActive(id)) {
            this.selectedSigns = this.selectedSigns.filter(signId => signId !== id);
        } else {
            this.selectedSigns.push(id);
        }

        if (this.selectedSigns.length === 3) {
            this.checkAnswer();
        }
    }

    isActive(id: number): boolean {
        return this.selectedSigns.includes(id);
    }

    private checkAnswer() {
        this.showLoader$.next(true);
        this.miniGameService
            .checkAnswer(this.getAnswers())
            .pipe(
                tap(({rightAnswer}) => {
                    this.right = rightAnswer;

                    setTimeout(() => {
                        this.right = null;
                        this.cd.markForCheck();
                    }, SHOW_RESULT_TIME);

                    this.cd.markForCheck();
                }),
                switchMap(() => this.miniGameService.load())
            )
            .subscribe(game => {
                this.game = game;
                this.showLoader$.next(false);
                this.selectedSigns = [];
                this.cd.markForCheck();
            });
    }

    private getAnswers(): IMiniGameAnswer {
        return {
            sign: this.selectedSigns
        };
    }

    private subscribeOnInitGame() {
        merge(this.userStateService._user$, this.init$)
            .pipe(
                distinctUntilChanged(),
                switchMap(() => this.miniGameService.load())
            )
            .subscribe(({timer, ready, ...game}) => {
                if (ready) {
                    this.timer = null;
                    this.game = {...game, ready};
                } else {
                    this.timer = new Date(timer);
                }

                this.cd.markForCheck();
            });
    }
}
