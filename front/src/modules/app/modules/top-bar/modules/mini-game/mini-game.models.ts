export interface IMiniGame extends IMiniGameReadyInfo {
    attemptNumber: number;
    question: string;
    signs: IMiniGameSign[];
}

export interface IMiniGameReadyInfo {
    ready: boolean;
    timer?: number;
}

export interface IMiniGameSign {
    id: number;
    url: string;
}

export interface IMiniGameAnswer {
    sign: number[];
}

export interface IMiniGameCheckAnswer {
    rightAnswer: boolean;
}
