import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MiniGameComponent} from './mini-game.component';
import {LoaderModule} from '../../../../../../shared/modules/loader/loader.module';

@NgModule({
    imports: [CommonModule, LoaderModule],
    declarations: [MiniGameComponent],
    exports: [MiniGameComponent]
})
export class MiniGameModule {}
