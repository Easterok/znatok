import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {IReview} from '../../interfaces/ireview';
import {MatDialog} from '@angular/material';
import {ReviewsDialogComponent} from '../reviews-dialog/reviews-dialog.component';
import {ReviewsService} from '../../services/reviews.service';
import {Observable} from 'rxjs/Rx';

@Component({
    selector: 'pdd-reviews',
    templateUrl: './reviews.component.html',
    styleUrls: ['./reviews.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReviewsComponent implements OnInit {
    reviews: Observable<IReview[]>;
    disabled: Observable<boolean>;

    constructor(private dialog: MatDialog, private reviewsService: ReviewsService) {}

    ngOnInit() {
        this.reviewsService.getReviews();
        this.reviews = this.reviewsService._reviews;
        this.disabled = this.reviewsService._disabled;
    }

    openReviewsDialog() {
        this.dialog.open(ReviewsDialogComponent, {
            width: '450px',
            autoFocus: false
        });
    }

    moreReviews() {
        this.reviewsService.getReviews();
    }
}
