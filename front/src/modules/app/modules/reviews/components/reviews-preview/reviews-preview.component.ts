import {ChangeDetectionStrategy, Component, Input, OnChanges, OnInit} from '@angular/core';
import {IReview} from '../../interfaces/ireview';

@Component({
    selector: 'pdd-reviews-preview',
    templateUrl: './reviews-preview.component.html',
    styleUrls: ['./reviews-preview.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReviewsPreviewComponent {
    @Input() review: IReview;
}
