import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginDialogComponent} from '../../../top-bar/components/login-dialog/login-dialog.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {getErrors} from '../../../../../../shared/functions/formValidators';
import {ReviewsService} from '../../services/reviews.service';

@Component({
    selector: 'pdd-reviews-dialog',
    templateUrl: './reviews-dialog.component.html',
    styleUrls: ['./reviews-dialog.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReviewsDialogComponent {
    form: FormGroup;
    getErrors: Function;
    showAlert = false;

    constructor(
        private reviewsService: ReviewsService,
        private cd: ChangeDetectorRef,
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<LoginDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any
    ) {
        this.form = this.fb.group({
            stars: [5, [Validators.required, Validators.min(1)]],
            text: ['', [Validators.required, Validators.maxLength(300)]],
            checkbox: ['', Validators.requiredTrue]
        });

        this.getErrors = getErrors;
    }

    get rating(): string {
        switch (this.form.get('stars').value) {
            case 1:
                return 'Плохо';
            case 2:
                return 'Пойдет';
            case 3:
                return 'Нормально';
            case 4:
                return 'Хорошо';
            case 5:
                return 'Отлично';
        }
    }

    closeDialog() {
        this.dialogRef.close();
    }

    onSubmitReviews() {
        Object.keys(this.form.controls).forEach(key => {
            this.form.get(key).markAsTouched({onlySelf: true});
        });

        if (this.form.invalid) {
            return;
        }

        const {stars, text} = this.form.value;

        this.reviewsService.newReview({rating: stars, text}).subscribe(
            () => {
                this.closeDialog();
            },
            () => {
                this.showAlert = true;
                this.cd.markForCheck();
            }
        );
    }
}
