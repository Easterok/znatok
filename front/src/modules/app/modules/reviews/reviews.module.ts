import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReviewsComponent} from './components/reviews/reviews.component';
import {ReviewsPreviewComponent} from './components/reviews-preview/reviews-preview.component';
import {StarsModule} from '../../../../shared/modules/stars/stars.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RussianDateModule} from '../../../../pipes/russian-date.module';
import {MatDialogModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReviewsDialogComponent} from './components/reviews-dialog/reviews-dialog.component';
import {FocusedModule} from '@shared/directives/focused/focused.module';

@NgModule({
    imports: [
        CommonModule,
        StarsModule,
        ReactiveFormsModule,
        FormsModule,
        RussianDateModule,
        BrowserAnimationsModule,
        MatDialogModule,
        FocusedModule
    ],
    declarations: [ReviewsComponent, ReviewsPreviewComponent, ReviewsDialogComponent],
    entryComponents: [ReviewsDialogComponent],
    exports: [ReviewsComponent]
})
export class ReviewsModule {}
