import {IUserShort} from '../../../../../models/new-user.model';

export interface IReview {
    rating: number;
    date?: Date;
    text: string;
    city?: string;
    name?: string;
    avatar?: string;
    user?: IUserShort;
}

export interface IReviewDTO {
    data: IReview[];
    has_next_page: boolean;
}
