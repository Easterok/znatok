import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs/Rx';
import {IReview, IReviewDTO} from '../interfaces/ireview';
import {filter, tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ReviewsService {
    private disabled = new BehaviorSubject<boolean>(true);
    private reviews: IReview[];
    private reviews$ = new BehaviorSubject<IReview[]>(null);
    private counter = 1;

    constructor(private http: HttpClient) {}

    get _reviews(): Observable<IReview[]> {
        return this.reviews$.asObservable();
    }

    get _disabled(): Observable<boolean> {
        return this.disabled.asObservable();
    }

    newReview(review: IReview): Observable<IReview> {
        const options = {
            withCredentials: true
        };

        return this.http.post<IReview>('/api/review/', review, options).pipe(
            tap(() => {
                this.counter = 1;
                this.getReviews();
            })
        );
    }

    getReviews() {
        if (this.counter > 3) {
            return;
        }

        const options = {params: new HttpParams().set('page', this.counter.toString()), withCredentials: true};

        this.http
            .get<IReviewDTO>(`/api/review/`, options)
            .pipe(
                tap(() => (this.counter += 1)),
                filter(({data}) => data.length !== 0)
            )
            .subscribe(({data, has_next_page}) => {
                this.reviews = Array.prototype.concat(this.reviews, data).filter(review => !!review);
                this.reviews$.next(this.reviews);
                this.disabled.next(!has_next_page);
            });
    }
}
