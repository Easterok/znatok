import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-advertising',
    templateUrl: './advertising.component.html',
    styleUrls: ['./advertising.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdvertisingComponent {}
