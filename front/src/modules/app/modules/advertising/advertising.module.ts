import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdvertisingComponent} from './advertising.component';

@NgModule({
    imports: [CommonModule],
    declarations: [AdvertisingComponent],
    exports: [AdvertisingComponent]
})
export class AdvertisingModule {}
