import {ProfileComponent} from './components/profile.component';
import {Routes} from '@angular/router';
import {AchievementsComponent} from './modules/achievements/achievements.component';
import {SettingsComponent} from './modules/settings/settings.component';
import {InformationComponent} from './modules/information/information.component';
import {AccountsComponent} from './modules/accounts/accounts.component';
import {PurchasesComponent} from './modules/purchases/purchases.component';
import {RaffleComponent} from './modules/raffle/raffle.component';

export const routes: Routes = [
    {
        path: '',
        component: ProfileComponent,
        children: [
            {
                path: 'settings',
                component: SettingsComponent
            },
            {
                path: 'achievements',
                component: AchievementsComponent
            },
            {
                path: 'information',
                component: InformationComponent
            },
            {
                path: 'accounts',
                component: AccountsComponent
            },
            {
                path: 'purchases',
                component: PurchasesComponent
            },
            {
                path: 'raffle',
                component: RaffleComponent
            },
            {
                path: '',
                redirectTo: 'settings',
                pathMatch: 'full'
            }
        ]
    }
];
