import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProfileComponent} from './components/profile.component';
import {ProfileRoutingModule} from './profileRouting.module';
import {AchievementsModule} from './modules/achievements/achievements.module';
import {SettingsModule} from './modules/settings/settings.module';
import {AccountsModule} from './modules/accounts/accounts.module';
import {InformationModule} from './modules/information/information.module';
import {PurchasesModule} from './modules/purchases/purchases.module';
import {RaffleModule} from './modules/raffle/raffle.module';
import {AdvertisingModule} from '../app/modules/advertising/advertising.module';

@NgModule({
    imports: [
        CommonModule,
        AchievementsModule,
        SettingsModule,
        AccountsModule,
        InformationModule,
        PurchasesModule,
        RaffleModule,
        ProfileRoutingModule,
        AdvertisingModule
    ],
    declarations: [ProfileComponent],
    exports: [ProfileComponent]
})
export class ProfileModule {}
