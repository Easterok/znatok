import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'pdd-avatar-crop-dialog',
    templateUrl: './avatar-crop-dialog.component.html',
    styleUrls: ['./avatar-crop-dialog.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarCropDialogComponent {
    imageChangedEvent: any = '';
    croppedImage: any = '';

    // @ViewChild('canvas') canvas: ElementRef;

    constructor(
        private dialogRef: MatDialogRef<AvatarCropDialogComponent>,
        private sanitizer: DomSanitizer,
        @Inject(MAT_DIALOG_DATA) private data: any
    ) {
        this.imageChangedEvent = data.event;
    }

    closeDialog() {
        this.dialogRef.close();

        const asd = '';
    }

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }
    imageCropped(image: string) {
        this.croppedImage = image;
    }
    imageLoaded() {
        // show cropper
    }
    loadImageFailed() {
        // show message
    }

    // mergeImages(image: string): SafeUrl {
    //     const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    //     const context = canvas.getContext('2d');
    //
    //     const image1 = new Image();
    //     const image2 = new Image();
    //
    //     image2.src = `url('${image}')`;
    //
    //     canvas.width = image1.width;
    //     canvas.height = image1.height;
    //
    //     context.globalAlpha = 1.0;
    //     context.drawImage(image1, 0, 0);
    //     context.globalAlpha = 0.5; //Remove if pngs have alpha
    //     context.drawImage(image2, 0, 0);
    //
    //     // image1.src = 'imgfile1.png';
    //
    //     return this.sanitizer.bypassSecurityTrustUrl(canvas.toDataURL());
    // }
}
