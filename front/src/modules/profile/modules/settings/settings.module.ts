import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SettingsComponent} from './settings.component';
import {DropdownButtonModule} from '../../../../shared/modules/dropdown-button/dropdown-button.module';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule, MatSlideToggleModule} from '@angular/material';
import {AvatarCropDialogComponent} from './components/avatar-crop-dialog/avatar-crop-dialog.component';

@NgModule({
    declarations: [SettingsComponent, AvatarCropDialogComponent],
    imports: [CommonModule, DropdownButtonModule, ReactiveFormsModule, MatSlideToggleModule, MatDialogModule],
    entryComponents: [AvatarCropDialogComponent],
    exports: [SettingsComponent]
})
export class SettingsModule {}
