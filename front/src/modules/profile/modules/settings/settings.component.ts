import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {getErrors} from '../../../../shared/functions/formValidators';
import {UserStateService} from '../../../../services/user-state.service';
import {Observable} from 'rxjs/Rx';
import {IUser} from '../../../../models/new-user.model';
import {MatDialog} from '@angular/material';
import {AvatarCropDialogComponent} from './components/avatar-crop-dialog/avatar-crop-dialog.component';

const IMAGE_VALIDATOR_MESSAGE = 'Разрешенный формат картинки .png либо .jpg/.jpeg';

@Component({
    selector: 'pdd-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsComponent implements OnInit {
    personalForm: FormGroup;
    passwordForm: FormGroup;
    getErrors: Function;
    doubleClickOnAnswer: boolean;
    user$: Observable<IUser>;
    imageChangedEvent: any = '';
    croppedImage: any = '';

    private readonly allowImageType = ['image/png', 'image/jpg', 'image/jpeg'];
    constructor(
        private formBuilder: FormBuilder,
        private userStateService: UserStateService,
        private dialog: MatDialog
    ) {
        this.user$ = this.userStateService._user$;

        this.personalForm = this.formBuilder.group({
            firstName: [''],
            secondName: [''],
            email: [''],
            username: [''],
            avatar: [''],
            birthday: [''],
            city: ['']
        });

        this.passwordForm = this.formBuilder.group({
            oldPassword: ['', [Validators.required]],
            newPassword: ['', [Validators.required]],
            newPasswordConfirm: ['', [Validators.required]]
        });

        this.getErrors = getErrors;
    }

    ngOnInit() {
        this.userStateService._user$.subscribe(
            ({first_name, last_name, email, username, user_avatar, birthday, city}) => {
                this.personalForm.setValue({
                    firstName: first_name,
                    secondName: last_name,
                    email,
                    username,
                    birthday,
                    city,
                    avatar: ''
                });
            }
        );

        this.doubleClickOnAnswer = this.userStateService.getDoubleClickOnAnswer();
        this.personalForm.get('email').disable();
        this.personalForm.get('username').disable();
    }

    onSubmitPersonalForm() {}

    onSubmitPasswordForm() {
        this.passwordForm.markAsTouched();
    }

    toggleChange() {
        this.doubleClickOnAnswer = !this.doubleClickOnAnswer;
        this.userStateService.setDoubleClickOnAnswer(this.doubleClickOnAnswer);
    }

    fileChangeEvent(event: any) {
        const file = event.srcElement.files[0];

        if (this.allowImageType.indexOf(file.type) < 0) {
            this.personalForm.get('avatar').setErrors({type: IMAGE_VALIDATOR_MESSAGE});
            event.srcElement.value = '';

            return;
        }

        this.imageChangedEvent = event;
        this.dialog.open(AvatarCropDialogComponent, {
            minHeight: '600px',
            width: '800px',
            disableClose: true,
            data: {event}
        });
    }
}
