import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PurchasesComponent} from './purchases.component';

@NgModule({
    imports: [CommonModule],
    declarations: [PurchasesComponent],
    exports: [PurchasesComponent]
})
export class PurchasesModule {}
