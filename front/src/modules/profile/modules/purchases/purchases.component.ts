import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-purchases',
    templateUrl: './purchases.component.html',
    styleUrls: ['./purchases.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PurchasesComponent {}
