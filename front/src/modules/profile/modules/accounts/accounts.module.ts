import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AccountsComponent} from './accounts.component';
import {DropdownButtonModule} from '../../../../shared/modules/dropdown-button/dropdown-button.module';

@NgModule({
    imports: [CommonModule, DropdownButtonModule],
    declarations: [AccountsComponent],
    exports: [AccountsComponent]
})
export class AccountsModule {}
