import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-accounts',
    templateUrl: './accounts.component.html',
    styleUrls: ['./accounts.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountsComponent {}
