import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RaffleComponent} from './raffle.component';

@NgModule({
    imports: [CommonModule],
    declarations: [RaffleComponent],
    exports: [RaffleComponent]
})
export class RaffleModule {}
