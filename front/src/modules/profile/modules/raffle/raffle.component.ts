import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-raffle',
    templateUrl: './raffle.component.html',
    styleUrls: ['./raffle.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RaffleComponent {}
