import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AchievementsComponent} from './achievements.component';
import {DropdownButtonModule} from '../../../../shared/modules/dropdown-button/dropdown-button.module';
import {ProgressBarModule} from '../../../../shared/modules/progress-bar/progress-bar.module';

@NgModule({
    imports: [CommonModule, DropdownButtonModule, ProgressBarModule],
    declarations: [AchievementsComponent],
    exports: [AchievementsComponent]
})
export class AchievementsModule {}
