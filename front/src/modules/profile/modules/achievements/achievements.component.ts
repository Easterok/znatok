import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {IUser} from '../../../../models/new-user.model';
import {UserStateService} from '../../../../services/user-state.service';

@Component({
    selector: 'pdd-achievements',
    templateUrl: './achievements.component.html',
    styleUrls: ['./achievements.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AchievementsComponent {
    cards = Array(8);
    user$: Observable<IUser>;

    constructor(private userStateService: UserStateService) {
        this.user$ = this.userStateService._user$;
    }
}
