import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-information',
    templateUrl: './information.component.html',
    styleUrls: ['./information.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InformationComponent {
    date = new Date(2018, 12, 12).toLocaleDateString();
}
