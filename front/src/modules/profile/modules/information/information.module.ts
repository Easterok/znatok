import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InformationComponent} from './information.component';
import {DropdownButtonModule} from '../../../../shared/modules/dropdown-button/dropdown-button.module';
import {RussianDateModule} from '../../../../pipes/russian-date.module';

@NgModule({
    imports: [CommonModule, DropdownButtonModule, RussianDateModule],
    declarations: [InformationComponent],
    exports: [InformationComponent]
})
export class InformationModule {}
