import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'pdd-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent {}
