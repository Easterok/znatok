export interface IQuestion {
    answers: string[];
    id: number;
    number: number;
    picture: string;
    question: string;
    result?: IResultQuestion;
}

export interface ITicket {
    id: number;
    number: number;
    questions?: IQuestion[];
    ticket_result?: {[key: string]: boolean};
    done?: boolean;
    error?: boolean;
}

export interface IResultQuestion {
    is_true: boolean;
    user_answer: string;
    true_answer: string;
}
