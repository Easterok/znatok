export interface INewUser {
    username: string;
    email: string;
    password: string;
}

export interface IUser {
    bio?: string;
    birthday?: string;
    city?: string;
    date_joined: string;
    email: string;
    first_name?: string;
    last_name?: string;
    last_password_update?: string;
    rank: number;
    rank_progress: number;
    user_avatar?: string;
    username: string;
    points: number;
    id?: number;
    token: string;
}

export interface IUserShort {
    points: number;
    city?: string;
    first_name?: string;
    last_name?: string;
    rank: number;
    user_avatar?: string;
    username: string;
}
